<!DOCTYPE html><html lang="en"><head>
  <script src="//assets.adobedtm.com/75133ce79ba1f2c516f7d5f09b683f779a12ac39/satelliteLib-2756bab2480cb6c2674fb064e707912a7e9432b2.js"></script>
  <script src="//www.ais.co.th/base_interface_v2/js/jquery-3.2.1.min.js"></script>
  <script src="//www.ais.co.th/base_interface/js/jquery.typeahead.min.js"></script>
  <script src="//www.ais.co.th/base_interface/js/menutypeahead.js"></script>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>AIS ให้คุณใช้ชีวิตได้มากกว่า ผู้นำเครือข่ายโทรศัพท์เคลื่อนที่ของประเทศไทย</title>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta http-equiv="X-UA-Compatible" content="IE=10,9" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0" />
  <!-- Html Meta -->
  <meta name="description" content="เอไอเอส ผู้ให้บริการเครือข่ายโทรศัพท์เคลื่อนที่รายเดือนและเติมเงิน รวมทั้งบริการต่างๆเกี่ยวกับอุปกรณ์มือถือ สมาร์ทโฟน iPhone อินเตอร์เน็ต WIFI และ AIS 4G คุณภาพ">
  <meta name="keywords" content="ais, เอไอเอส, มือถือ, เครือข่าย 4g, เครือข่ายอินเตอร์เน็ต, โทรศัพท์เคลื่อนที่, mobile operators thailand, iphone">
  <!-- Facebook Propertie -->
  <meta property="og:title" content="AIS ให้คุณใช้ชีวิตได้มากกว่า ผู้นำเครือข่ายโทรศัพท์เคลื่อนที่ของประเทศไทย" />
  <meta property="og:description" content="เอไอเอส ผู้ให้บริการเครือข่ายโทรศัพท์เคลื่อนที่รายเดือนและเติมเงิน รวมทั้งบริการต่างๆเกี่ยวกับอุปกรณ์มือถือ สมาร์ทโฟน iPhone อินเตอร์เน็ต WIFI และ AIS 4G คุณภาพ" />
  <meta property="og:image" content="share.jpg" />
  <!-- Google Plus -->
  <meta itemprop="name" content="AIS ให้คุณใช้ชีวิตได้มากกว่า ผู้นำเครือข่ายโทรศัพท์เคลื่อนที่ของประเทศไทย">
  <meta itemprop="description" content="เอไอเอส ผู้ให้บริการเครือข่ายโทรศัพท์เคลื่อนที่รายเดือนและเติมเงิน รวมทั้งบริการต่างๆเกี่ยวกับอุปกรณ์มือถือ สมาร์ทโฟน iPhone อินเตอร์เน็ต WIFI และ AIS 4G คุณภาพ">
  <meta itemprop="image" content="share.jpg">
  <!-- apple itunes application -->
  <meta name ="apple-itunes-app" content="app-id=399758084"/>
  <!-- Start SmartBanner configuration -->
  <meta name="smartbanner:title" content="my AIS">
  <meta name="smartbanner:author" content="AIS ให้คุณใช้ชีวิตได้มากกว่า">
  <meta name="smartbanner:price" content="FREE">
  <meta name="smartbanner:price-suffix-google" content=" Download Now">
  <meta name="smartbanner:icon-google" content="https://lh3.googleusercontent.com/zpXlAyDbHvn4Q7WijKM_HPMYTXOOAhVTkKqZQS3K-LX6-NX-QH-ghF4R-dQu2T-fcfls=w300-rw">
  <meta name="smartbanner:button" content="VIEW">
  <meta name="smartbanner:button-url-google" content="https://play.google.com/store/apps/details?id=com.ais.mimo.eservice&hl=en">
  <meta name="smartbanner:enabled-platforms" content="android">
  <meta name="smartbanner:disable-positioning" content="true">
  <meta name="smartbanner:hide-ttl" content="10000">
  <!-- ถ้าต้องการแสดง banner andriod ให้คอมเม้นบรรทัดด้านล่าง -->
  <meta name="smartbanner:enabled-platforms" content="none">
  <!-- End SmartBanner configuration -->
  <!--Include Style Script-->
  <link rel="stylesheet" href="//www.ais.co.th/base_interface_v2/css/navais_v2.css">
  <style>

  </style>
</head>
<body>
<!--ais_topbar-->
<div id="ais_topbar"></div>
<!--ais_topbar-->
<div id="container">
  <!--  Put Content Here เริ่มต้นเนื้อหา  -->
  <div id="box-demo">Content Area</div>
  <!--  Put Content Here จบเนื้อหา  -->
</div>
<!--aisfooter-->
<div id="aisfooter"></div>
<!--aisfooter-->
<script src="//www.ais.co.th/base_interface_v2/js/jquery.nav.control.js"></script>
<link rel="stylesheet" href="//www.ais.co.th/base_interface_v2/fonts/fonts.css">
<link rel="stylesheet" href="//www.ais.co.th/base_interface/css/jquery.typeahead.css">
<link rel="stylesheet" href="//www.ais.co.th/base_interface/css/menutypeahead.css">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TC3PWS"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-TC3PWS');</script>
<!-- End Google Tag Manager -->
<script type="text/javascript">__th_page = "ais";</script>
<script type="text/javascript" src="//hits.truehits.in.th/data/s0029135.js"></script>
<script type="text/javascript" src="https://lvs.truehits.in.th/datasecure/s0029135.js"></script>
<script>
  $(function() {

    ais_interface_setup('mini-menu','home','th','','');

    digitalData = {
      page:{
        pageInfo:{
          pageName:"ais:th:home",
          site: "ais",
          language: "th"
        },
        category:{
          pageType:"home",
          primaryCategory:"home",
        }
      },
    };

  });
</script>
<script type="text/javascript">_satellite.pageBottom();</script>
</body>
</html>
