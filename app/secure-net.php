<?php
include_once('ais-header.php');
?>

  <div class="page-nav page-nav-secure-net">
    <?php include_once('dev-main-nav-part.php'); ?>
    <div class="page-nav__bottom">
      <button class="page-nav-mb-btn" type="button"><span class="text"></span><span class="icon"><img src="./img/down-arrow.png"></span></button>
      <div class="container mb-dropdown">
        <ul class="page-nav__bottom__content">
          <li class="page-nav-item"><a href="#home">AIS Secure Net</a></li>
          <li class="page-nav-item"><a href="#services">ลักษณะบริการ</a></li>
          <li class="page-nav-item"><a href="#how-to">วิธีการใช้บริการ</a></li>
          <li class="page-nav-item"><a href="#packages">แพ็กเกจและวิธีสมัคร</a></li>
          <li class="page-nav-item"><a href="#faq">คำถามที่พบบ่อย</a></li>
        </ul>
      </div>
    </div>
  </div>

  <main role="main" class="flex-shrink-0 page-secure">

    <a href="javascript:void(0);" onclick="openPopupById('popup-subscribe')" target="_blank" id="btn-subscribe"></a>

    <!-- Section : Hero-->
    <div id="home">

      <div class="page-secure__hero theme--light">
        <div class="container">
          <div class="_inner" data-aos="fade-up">
            <h1 class="text-primary">AIS Secure Net</h1>
            <p class="_desc">
              บริการที่ช่วยปกป้องคุณจากภัยไซเบอร์<br>
              แจ้งเตือนและบล็อกการเข้าถึงเว็บไซต์ที่สุ่มเสี่ยงพบไวรัส มัลแวร์<br>
              หรือภัยคุกคามไซเบอร์อื่นๆ และเว็บไซต์ไม่เหมาะสม ผ่านเครือข่าย<br>
              AIS 4G ADVANCED และ AIS 3G โดยไม่ต้องลงแอปพลิเคชันใดๆ
            </p>

            <h3>บริการมีให้เลือกใช้งาน 2 แพ็กเกจ</h3>
            <div class="flex-container">
              <div class="flex-col" data-aos="fade-right" data-aos-delay="300">
                <h2>AIS Secure Net Kids</h2>
                <p>เหมาะสำหรับกลุ่มเด็กเล็กอายุระหว่าง 5-12 ปี</p>
              </div>
              <div class="flex-col" data-aos="fade-left" data-aos-delay="300">
                <h2>AIS Secure Net</h2>
                <p>เหมาะสำหรับกลุ่มผู้ใช้งานทั่วไป</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <!-- Section : DQ8 -->
    <div id="services">
      <div class="page-secure__services theme--light">
        <div class="container">
          <h2>ลักษณะบริการ</h2>
          <div class="feature-container">
            <!-- feature -->
            <div class="feature">
              <div class="flex-container">
                <img src="./img/secure-net/snf-icon-1.png"/>
                <div class="feature__content">
                  <h4>แจ้งเตือนและบล็อกการเข้าถึงเว็บไซต์สุ่มเสี่ยงจากภัยคุกคามไซเบอร์</h4>
                  <div class="full-mobile">
                    <p>
                      ป้องกัน ตรวจจับ และกรองเว็บไซต์ที่สุ่มเสี่ยงและมีภัยคุกคามไซเบอร์จากการใช้งานอินเทอร์เน็ต
                      บนบราวเซอร์ โดยบริการจะแจ้งเตือนและบล็อก เมื่อมีการเข้าถึงเว็บไซต์ที่สุ่มเสี่ยงจากภัยคุกคาม
                      ไซเบอร์ ได้แก่
                    </p>
                    <div class="feature__hidden">
                      <ul>
                        <li>Hacking/Computer Crime</li>
                        <li>Spam URLs</li>
                        <li>Illegal Software</li>
                        <li>Phishing</li>
                        <li>Malicious Sites</li>
                        <li>Viruses</li>
                        <li>Spyware/Adware</li>
                        <li>Remote Access Tools</li>
                      </ul>
                      <h5>
                        *บริการสามารถให้คุณกด ข้ามหน้าบล็อก โดยกด “ยอมรับและเข้าใช้งาน” เพื่อเข้าสู่เว็บไซต์ที่ระบบได้บล็อกไว้
                        (ใช้ได้เฉพาะแพ็กเกจ AIS Secure Net เท่านั้น)
                      </h5>
                    </div>
                    <hr>
                  </div>
                </div>
              </div>
            </div>
            <!-- feature -->
            <div class="feature">
              <div class="flex-container">
                <img src="./img/secure-net/snf-icon-2.png"/>
                <div class="feature__content">
                  <h4>แจ้งเตือนและบล็อกการเข้าถึงเว็บไซต์ที่ไม่เหมาะสม</h4>
                  <div class="full-mobile">
                    <p>
                      ป้องกันการเข้าถึงเว็บไซต์ที่ไม่เหมาะสมสำหรับเด็กและเยาวชนจากการใช้งานอินเทอร์เน็ตบน
                      บราวเซอร์ โดยบริการจะแจ้งเตือนและการบล็อก เมื่อมีการเข้าถึงเว็บไซต์ที่ไม่เหมาะสม ได้แก่
                    </p>
                    <div class="feature__hidden">
                      <ul>
                        <li>กลุ่มเว็บไซต์ลามกอนาจาร</li>
                        <li>กลุ่มเว็บไซต์ที่เกี่ยวกับแอลกอฮอล์ สิ่งเสพติด และยาสูบ</li>
                        <li>กลุ่มเว็บไซต์อาวุธและความรุนแรง</li>
                        <li>กลุ่มเว็บไซต์แบบ peer to peer</li>
                        <li>กลุ่มเว็บไซต์หาคู่</li>
                        <li>กลุ่มเว็บไซต์ที่เกี่ยวกับการดูหมิ่น</li>
                        <li>กลุ่มเว็บไซต์การพนันและเกมส์ที่มีการเดิมพัน</li>
                        <li>กลุ่มเว็บไซต์ที่เกี่ยวกับโจรกรรมออนไลน์ (Hacking)</li>
                        <li>กลุ่มเว็บไซต์ความเชื่อ โหราศาสตร์ เวทมนตร์เหนือธรรมชาติ หรือเชื้อชาติ</li>
                      </ul>
                    </div>
                  </div>
                  <hr>
                </div>
              </div>
            </div>
            <!-- feature -->
            <div class="feature">
              <div class="flex-container">
                <img src="./img/secure-net/snf-icon-3.png"/>
                <div class="feature__content">
                  <h4>การเรียกดูรายงาน</h4>
                  <div class="full-mobile">
                    <p>
                      ตรวจสอบเว็บไซต์ที่สามารถป้องกันและบล็อกได้ด้วยบริการ AIS Secure Net
                    </p>
                    <ul class="_outside">
                      <li>รายงานของแพ็กเกจ AIS Secure Net Kids</li>
                      <li>รายงานของแพ็กเกจ AIS Secure Net</li>
                    </ul>
                  </div>
                  <hr>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : How to -->
    <div id="how-to">
      <div class="page-secure__how-to theme--light">
        <div class="container">
          <h2>วิธีการใช้บริการ</h2>
          <div class="btn-group ts-tabs ts-tabs--primary _custom-how-to text-medium" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-outline-tab active" data-tab="#tab-1"><span>AIS Secure Net Kids</span></button>
            <button type="button" class="btn btn-outline-tab" data-tab="#tab-2"><span>AIS Secure Net</span></button>
          </div>

          <!-- tab kids -->
          <div class="tab-content-container">
            <div id="tab-1" class="tab-content active">
              <div class="carousel-wrap">
                <!-- carousel kids - 1 -->
                <div class="carousel-item">
                  <div class="flex-container">
                    <div class="flex-col _col-1-kids">
                      <div class="heading-wrap">
                        <h3 class="heading-top">แจ้งเตือนและบล็อก</h3>
                        <div class="line"></div>
                        <h3 class="heading-bottom">เว็บไซต์ที่ไม่เหมาะสม</h3>
                      </div>

                      <ol class="steps">
                        <li data-img="kids-1-1" class="active"><span>พิมพ์ URL หรือกดเข้าเว็บไซต์</span></li>
                        <li data-img="kids-1-2"><span>ระบบแจ้งเตือนและบล็อก เมื่อเข้าเว็บไซต์ที่ไม่เหมาะสม</span></li>
                        <li data-img="kids-1-3"><span>กด “ย้อนกลับ” เพื่อเข้าใช้งานบราวเซอร์ อื่นๆ</span></li>
                      </ol>
                    </div>
                    <div class="flex-col">
                      <div class="phone-wrap">
                        <div class="phone-display">
                          <img id="kids-1-1" src="./img/secure-net/kids/1/1.jpg" alt=""/>
                          <img id="kids-1-2" src="./img/secure-net/kids/1/2.jpg" alt=""/>
                          <img id="kids-1-3" src="./img/secure-net/kids/1/3.jpg" alt=""/>
                        </div>
                        <div class="phone-mask"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- carousel kids - 2 -->
                <div class="carousel-item">
                  <div class="flex-container">
                    <div class="flex-col _col-1-kids">
                      <div class="heading-wrap">
                        <h3 class="heading-top">แจ้งเตือนและบล็อก</h3>
                        <div class="line"></div>
                        <h3 class="heading-bottom">เว็บไซต์ที่สุ่มเสี่ยงภัยคุกคามไซเบอร์</h3>
                      </div>
                      <ol class="steps">
                        <li data-img="kids-2-1" class="active"><span>พิมพ์ URL หรือกดเข้าเว็บไซต์</span></li>
                        <li data-img="kids-2-2"><span>ระบบแจ้งเตือนและบล็อก เมื่อเข้าเว็บไซต์ ที่สุ่มเสี่ยงจากภัยคุกคามไซเบอร์</span></li>
                        <li data-img="kids-2-3"><span>กด “ย้อนกลับ” เพื่อเข้าใช้งานบราวเซอร์ อื่นๆ</span></li>
                      </ol>
                    </div>
                    <div class="flex-col">
                      <div class="phone-wrap">
                        <div class="phone-display">
                          <img id="kids-2-1" src="./img/secure-net/kids/2/1.jpg" alt=""/>
                          <img id="kids-2-2" src="./img/secure-net/kids/2/2.jpg" alt=""/>
                          <img id="kids-2-3" src="./img/secure-net/kids/2/3.jpg" alt=""/>
                        </div>
                        <div class="phone-mask"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="tab-2" class="tab-content">
              <div class="flex-container">
                <div class="flex-col _col-1-net">
                  <div class="heading-wrap">
                    <h3 class="heading-top">แจ้งเตือนและบล็อก</h3>
                    <div class="line"></div>
                    <h3 class="heading-bottom">เว็บไซต์ที่สุ่มเสี่ยงภัยคุกคามไซเบอร์</h3>
                  </div>
                  <ol class="steps">
                    <li data-img="net-1" class="active"><span>พิมพ์ URL หรือกดเข้าเว็บไซต์</span></li>
                    <li data-img="net-2"><span>ระบบแจ้งเตือนและบล็อก เมื่อเข้าเว็บไซต์ที่สุ่มเสี่ยงจากภัยคุกคามไซเบอร์</span>์</li>
                    <li data-img="net-3"><span>กด “ย้อนกลับ” เพื่อเข้าใช้งานบราวเซอร์อื่นๆ</span></li>
                    <!-- <li data-img="net-4"><span>กด “ยอมรับและเข้าใช้งาน” เพื่อดำเนินการต่อ</span></li> -->
                  </ol>
                  <p class="note">
                    *ในกรณีนี้ ผู้ใช้งานยอมรับความเสี่ยงของ
                    เว็บไซต์ที่กำลังเข้าและปฏิเสธการ
                    บล็อกจากบริการ</p>

                </div>
                <div class="flex-col">
                  <div class="phone-wrap">
                    <div class="phone-display">
                      <img id="net-1" src="./img/secure-net/net/1.jpg" alt=""/>
                      <img id="net-2" src="./img/secure-net/net/2.jpg" alt=""/>
                      <img id="net-3" src="./img/secure-net/net/3.jpg" alt=""/>
                      <!-- <img id="net-4" src="./img/secure-net/net/4.jpg" alt=""/> -->
                    </div>
                    <div class="phone-mask"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="subscribe-btn-container">
          <button class="btn btn-dark" onclick="openPopupById('popup-subscribe')" type="button"><span>ลงทะเบียนแจ้งความสนใจใช้ AIS Secure Net ก่อนใคร</span></button>
        </div>
      </div>
    </div>

    <!-- Section : Package -->
    <div id="packages">
      <div class="page-secure__packages theme--light">
        <div class="container">
          <div class="_inner">

            <h2>แพ็กเกจ Secure Net</h2>
            <div class="row">
              <div class="col-lg-5">
                <div class="card-black">
                  <h3>AIS <span class="color-white">Secure Net Kids</span></h3>
                  <div class="card-black__detail">
                    แพ็กสำหรับกลุ่มเด็กเล็ก 5-12 ปี
                  </div>
                  <div class="card-black__howTo">
                    กด <span class="color-green"><span class="asterisk">*</span>689<span class="asterisk">*</span>5#</span> <img class="img-tel" src="./img/family-link/how-to-register/telephone.png">
                  </div>
                </div>
              </div>
              <div class="col-lg-5">
                <div class="card-black">
                  <h3>AIS <span class="color-white">Secure Net</span></h3>
                  <div class="card-black__detail">
                    แพ็กสำหรับกลุ่มลูกค้าทั่วไป
                  </div>
                  <div class="card-black__howTo">
                    กด <span class="color-green"><span class="asterisk">*</span>689<span class="asterisk">*</span>6#</span> <img class="img-tel" src="./img/family-link/how-to-register/telephone.png">
                  </div>
                </div>
              </div>
            </div>
            <div class="package-link">
              <a target="_blank" href="https://families.google.com/intl/th_ALL/familylink/privacy/notice">เงื่อนไขการให้บริการ</a>
            </div>

            <!-- <h2>พร้อมให้บริการเร็วๆ นี้</h2> -->
          </div>
        </div>
      </div>
    </div>

    <!-- Section : FAQ -->
    <div id="faq">
      <div class="page-secure__faq sec-faq theme--light">
        <div class="container">
          <h2>คำถามที่พบบ่อย</h2>
          <div class="faq-container">

            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>ถ้าใช้งานผ่านแอปพลิเคชันจะสามารถป้องกันได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  สามารถปกป้องได้เฉพาะบางแอปพลิเคชัน ที่ทำงานโดยการเรียกถาม IP จากระบบ DNS ดังนั้นแอปพลิเคชันในกลุ่มนี้จะถูกปกป้องได้ด้วยบริการ AIS Secure Net Kids และ AIS Secure Net แต่ในแอปพลิเคชันจะไม่ปรากฏหน้าบล็อก (Blocking Page) ขึ้นมาบล็อกการใช้งานและในบางแอปพลิเคชันจะพบว่าไม่ปรากฏคอนเท็นท์ใดๆเลย
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>หากใช้งานเว็บไซต์ผ่านทาง WiFi จะได้รับการปกป้องหรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>ไม่ได้ บริการนี้จะสามารถปกป้องการใช้งานผ่านบราวเซอร์เมื่อใช้งานผ่านเครือข่าย AIS 4G ADVANCED และ AIS 3G เท่านั้น โดยในอนาคตจะสามารถรองรับ AIS Super WiFi และ AIS Fibre<br>
                  ** กรณีที่ใช้งานเครื่องสองซิมและสองผู้ให้บริการ บริการจะใช้งานได้เมื่อเลือก AIS เป็นซิมหลักในการใช้งานดาต้า                  
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>สามารถใช้บริการในต่างประเทศได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>ไม่สามารถใช้งานในต่างประเทศได้  หากนำไปใช้งานจะไม่ได้รับการปกป้องจากบริการ</p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>หากพบบางเว็บไซต์มีความสุ่มเสี่ยงที่เด็กจะเข้าไปใช้งาน แต่ยังไม่ได้รับการกรองหรือป้องกันจากการบริการ AIS Secure Net ต้องทำอย่างไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>สามารถโทรติดต่อ 1175 เพื่อแจ้งเว็บไซต์ที่ต้องการให้กรองเพิ่มเติม
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>ยกเลิกบริการอย่างไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>คุณสามารถยกเลิกทั้ง 2 แพ็กเกจ ได้ดังนี้</p>
                <ul>
                  <li>AIS Secure Net Kids - ยกเลิกผ่านการกด *689*51* หมายเลขบัครประชาชน 6 หลักท้าย#</li>
                  <li>AIS Secure Net – ยกเลิกผ่านทางการกด *689*61# หรือผ่านแอปพลิเคชัน myAIS</li>
                </ul>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>บริการนี้สามารถใช้ได้กับทุกซิมหรือทุกเบอร์หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>บริการนี้สมัครได้เฉพาะเบอร์ลูกค้าทั่วไปเท่านั้น ยกเว้น Traveller SIM, SIM2Fly และกลุ่ม Asian SIM (เช่น Myanmar SIM และ Cambodian SIM)
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>รองรับการใช้งานได้ทุกๆ อุปกรณ์หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>รองรับทุกอุปกรณ์และระบบปฏิบัติการ ทั้ง iOS, Android และ Window Phone
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>หากเครื่องของลูกค้าติดไวรัสหรือมัลแวร์ก่อนใช้บริการนี้ หลังจากสมัครใช้บริการแล้วจะสามารถกำจัดไวรัสหรือมัลแวร์ดังกล่าวได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ไม่สามารกำจัดไวรัสหรือมัลแวร์ที่ติดมากับเครื่องก่อนสมัครใช้บริการได้ แต่สามารถตัดการเชื่อมต่อระหว่างไวรัสหรือมัลแวร์ที่เครื่องไปยังเซิร์ฟเวอร์ปลายทาง
                  และจะช่วยปกป้องไม่ให้ติดไวรัสหรือมัลแวร์อื่นๆเพิ่มเติมผ่านทางเว็บไซต์
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>เมื่อสมัครบริการแล้วอยากเลือกปิดคุณสมบัติการกรองเว็บไซต์ที่ไม่เหมาะสมหรือคุณสมบัติการป้องกันไวรัสหรือมัลแวร์อย่างใดอย่างหนึ่ง หรือทั้งสองอย่างได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>ไม่สามารถเลือกปิดเฉพาะบางคุณสมบัติได้ เบื้องต้นบริการได้ตั้งค่าพื้นฐานการกรองให้เหมาะสมสำหรับกลุ่มเด็กเล็กในและกลุ่มผู้ใช้งานทั่วไป
                </p>
              </div>
            </div>
            <!-- question -->
            <!-- <div class="ts-faq">
              <div class="ts-faq__q">
                <p>บริการนี้มีการคิดค่าบริการอย่างไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>ลูกค้าใช้บริการฟรีถึง 31 ก.ค. 2563
                </p>
              </div>
            </div> -->
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>การใช้บริการหรือการเข้าถึงเว็บไซต์ต่างๆ จะกระทบความเป็นส่วนตัวของผู้ใช้งานหรือไม่? </p>
              </div>
              <div class="ts-faq__a">
                <p>ข้อมูลส่วนบุคคลของท่าน ได้แก่ IP เว็บไซต์ หรือลิ้งค์ปลายทาง จะถูกจัดเก็บ เพื่อให้ระบบทำการตรวจสอบและเปรียบเทียบกับฐานข้อมูลของบริการ ทั้งนี้เพื่อให้การบริการ AIS Secure Net Kids และ
                  AIS Secure Net เป็นไปได้อย่างสมบูรณ์และปลอดภัย แต่ข้อมูลจะไม่ถูกเปิดเผย จึงไม่กระทบต่อนโยบายความเป็นส่วนตัวของผู้ใช้งาน
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>หากนำหมายเลขที่สมัครบริการไปแชร์อินเทอร์เน็ตให้กับอุปกรณ์อื่น อุปกรณ์นั้นยังจะได้รับการปกป้องหรือไม่? </p>
              </div>
              <div class="ts-faq__a">
                <p>อุปกรณ์นั้นยังได้รับการปกป้องเช่นเดียวกับหมายเลขที่สมัครบริการ แต่อย่างไรก็ตามควรสมัครบริการให้ครบทุกๆอุปกรณ์ที่ใช้งาน เพื่อความสะดวกและลดข้อผิดพลาดในการใช้งาน
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>บริการใดบ้างที่ไม่สามารถใช้ร่วมกับ AIS Secure Net ได้?</p>
              </div>
              <div class="ts-faq__a">
                <ul>
                  <li>AIS NEXT G</li>
                  <li>MultiSIM และ MultiSIM PLUS</li>
                  <li>การใช้งานผ่านทางเครือข่ายเสมือน หรือ VPN</li>
                </ul>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>การใช้บริการนี้มีผลกระทบต่ออุปกรณ์สมาร์ทโฟนบ้างหรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>ไม่ส่งผลกระทบใดๆ เนื่องจากการแจ้งเตือนและการบล็อกเว็บไซต์กระทำผ่านเครือข่าย AIS 4G ADVANCED และ AIS 3G เท่านั้น
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>การใช้บริการนี้คิดค่าใช้งานดาต้าเพิ่มเติมหรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>ไม่
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>ฟังก์ชั่น Safe search คืออะไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>ในแพ็กเกจ AIS Secure Net Kids จะพบฟังก์ชั่น Safe Search ที่สามารถกรอง Key word ที่ไม่เหมาะสมสำหรับการใช้งาน search engine เช่น Bing Google Youtube และ DuckduckGo
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>ระหว่างการใช้งานพบหน้าบล็อกหรือคำเตือน ที่ไม่ใช่หน้าบล็อกของบริการจะต้องทำอย่างไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>เมื่อพบหน้าบล็อกหรือคำเตือนต่างๆ ไม่ว่าจะมาจากเบราว์เซอร์ที่คุณใช้ หรือ มาจากหน่วยงานรัฐอื่นใด คุณควรหลีกเลี่ยงการเข้าใช้งานเว็บไซต์นั้นๆ  หรือหากคุณกดยอมรับเข้าใช้งานจากคำเตือนที่พบ ระบบจะทำการบล็อกเว็บไซต์เหล่านั้นให้อีกชั้น เพื่อความปลอดภัยสูงสุดของคุณ
                </p>
              </div>
            </div>
            <!-- question -->
            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>บริการนี้จะช่วยบล็อกรูปภาพ วิดีโอสตรีมมิ่ง ที่มีเนื้อหาไม่เหมาะสมบนเว็บไซต์หรือแอปพลิเคชันได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>ได้ ในกรณีที่เว็บไซต์หรือแอปพลิเคชันนั้นอยู่ในระบบฐานข้อมูลและถูกกำหนดการบล็อกไว้ แต่สำหรับเว็บไซต์หรือแพลตฟอร์มสาธารณะที่ให้บริการแลกเปลี่ยนภาพหรือวิดีโอ เช่น Youtube, Facebook, Instagram และอื่นๆ ระบบไม่สามารถบล็อกได้ (เนื้อหาที่เป็นรูปภาพ คลิป หรือวิดีโอสตรีมมิ่งของผู้ใช้งานแต่ละรายที่อัพโหลดลงบนเว็บไซต์หรือแพลตฟอร์มสาธารณะ เป็นข้อมูลส่วนบุคคล ระบบไม่สามารถเข้าถึงและบล็อกเนื้อหาเหล่านั้นได้)
                </p>
              </div>
            </div>


            <!-- end all question -->
          </div>
        </div>
      </div>
    </div>

  </main>

  <?php include_once('popups/popup-subscribe.php') ?>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
  <script>
    $(document).ready(function () {

      // Features
      // ----------------------------------------

      $('.feature').each(function () {
        var me = $(this);
        var $hide = me.find('.feature__hidden');
        if ($hide.length > 0) {
          me.addClass('has_toggle');
          $hide.hide();
          me.click(function () {
            $hide.slideToggle();
            me.toggleClass('active');
          });
        }
      });

      // How to
      // ----------------------------------------

      $('.steps li').hover(function () {
          var me = $(this);
          me.addClass('active').siblings().removeClass('active');
          $('#' + me.data('img')).first().show().siblings().hide()
        },
        // leave
        function () {
          $(this).removeClass('active');
        });

      // default image
      $('.steps li.active').each(function () {
        $('#' + $(this).data('img')).first().show().siblings().hide();
      });

      $('.carousel-wrap').slick({
        dots: true,
        infinite: false,
        prevArrow: '<div class="arrow-prev"></div>',
        nextArrow: '<div class="arrow-next"></div>',
      });

      // active menu
      $('.page-nav__top>.page-nav-item:eq(3)>a').addClass('active');
    });
  </script>

<?php include_once('ais-footer.php');
