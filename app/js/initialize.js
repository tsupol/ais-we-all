
(function ($) {

  // Scroll
  // ----------------------------------------
  var position = null;
  var scrollCount = 0;
  var debouceRemoveScrolling = debounce(function () {
    scrollCount = 0;
    $('body').removeClass('scrolling');
  }, 1500);

  function debounce (func, wait, immediate) {
    var timeout;
    return function () {
      var context = this, args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  var mobileBreakPoint = 992; // 768
  var $contentWrapper = $('#main').first();
  var $backToTop = $('#back-to-top');
  function scrollFunction () {
    // Scroll distance classes
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
      document.body.classList.add('scrolled-nav');
      if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        document.body.classList.add('scrolled-far');
      } else {
        document.body.classList.remove('scrolled-far');
      }
      $backToTop.removeClass('hidden')
    } else {
      document.body.classList.remove('scrolled-nav');
      $backToTop.addClass('hidden')
    }


    if (window.innerWidth <= mobileBreakPoint && $contentWrapper.length) {
      var pos = $contentWrapper.position().top + $contentWrapper.outerHeight(true) - window.innerHeight;
      if(document.documentElement.scrollTop > pos || document.body.scrollTop > pos) {
        document.body.classList.add('scrolled-gt-content');
      } else {
        document.body.classList.remove('scrolled-gt-content');
      }
    } else {
      document.body.classList.remove('scrolled-gt-content');
    }


    // Check scrolling
    var scroll = $(window).scrollTop();
    if (scroll !== position && position != null) {
      scrollCount += Math.abs(scroll - position);
      if (scrollCount > 300) {
        $('body').addClass('scrolling');
      }
      debouceRemoveScrolling();
    }
    position = scroll;
  }


  $(window).scroll(function () {
    scrollFunction();
  });

  // Disable Hover
  // ----------------------------------------
  function hasTouch () {
    return 'ontouchstart' in document.documentElement
      || navigator.maxTouchPoints > 0
      || navigator.msMaxTouchPoints > 0;
  }

  if (hasTouch()) { // remove all :hover stylesheets
    try { // prevent exception on browsers not supporting DOM styleSheets properly
      for (var si in document.styleSheets) {
        var styleSheet = document.styleSheets[si];
        if (!styleSheet.rules) continue;

        for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
          if (!styleSheet.rules[ri].selectorText) continue;

          if (styleSheet.rules[ri].selectorText.match(':hover')) {
            styleSheet.deleteRule(ri);
          }
        }
      }
    } catch (ex) {
    }
  }

  // trigger resize event
  // ----------------------------------------
  if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
    var evt = document.createEvent('UIEvents');
    evt.initUIEvent('resize', true, false, window, 0);
    window.dispatchEvent(evt);
  } else {
    window.dispatchEvent(new Event('resize'));
  }

  // Tab
  // ----------------------------------------
  var tabs = $('.ts-tabs').each(function() {
    var me = $(this);
    var childrenWidth = 0;
    me.children().each(function () {
      childrenWidth += $(this).width();
    });
    if(childrenWidth < me.width() - 17) {
      me.addClass('mobile-fill');
    }
  });

})(jQuery);
