$(document).ready(function () {

  var duration = 600;
  var delayStart = 0;
  var delayStep = 200;
  var easing = 'cubic-bezier(0.19, 1, 0.22, 1)';

  var i = 0;

  var animations = {
    'fade_up': {
      'data-aos': 'fade-up',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
    },
    'fade_in': {
      'data-aos': 'fade-in',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
    },
    'zoom_in': {
      'data-aos': 'zoom-in',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
    },
  };


  // Page Home
  // ----------------------------------------
  var $pageHome = $('.page-home');
  if($pageHome.length > 0) {

    $pageHome.find('#sec-desc').find('h2').attr(animations.fade_up).siblings('img').attr(animations.fade_in);

    $pageHome.find('#sec-wellness').find('.line-left').attr({
      'data-aos': 'reveal-down',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });

    $pageHome.find('#sec-educator').find('.line').attr({
      'data-aos': 'reveal-left',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });

    $pageHome.find('#sec-e-waste').find('.line').attr({
      'data-aos': 'reveal-left',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    })

  }

  // Page Secure Net
  // ----------------------------------------
  var $pageSecureNet = $('.page-secure');
  if($pageSecureNet.length > 0) {
    // For 'hero' section, you have to add it manually

    var $secServices = $pageSecureNet.find('#services');
    $secServices.find('.feature').attr(animations.fade_up);

    $pageSecureNet.find('#packages h2').attr(animations.zoom_in);

    var $secHowTo = $pageSecureNet.find('#how-to');
    $secHowTo.find('h2').attr({
      'data-aos': 'fade-up',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });
    $secHowTo.find('.tab-content-container').attr({
      'data-aos': 'fade-down',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });

    i = 0;
    // only first 5
    $pageSecureNet.find('#faq .ts-faq').slice(0,5).each(function () {
      $(this).attr({
        'data-aos': 'flip-up',
        'data-aos-easing': easing,
        'data-aos-duration': duration,
        'data-aos-delay': delayStart + 100 * i++,
        'data-aos-once': true,
      });
    });
  }

  // Page DQ
  // ----------------------------------------
  var $pageDq = $('.page-dq');
  if($pageDq.length > 0) {

    $pageDq.find('#dq8').find('h2').attr(animations.fade_up).siblings('img').attr(animations.fade_in);

    var $secBenefits = $pageDq.find('#benefit');
    $secBenefits.find('h2').attr({
      'data-aos': 'fade-up',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });
    $secBenefits.find('#tab-1').attr({
      'data-aos': 'fade-down',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });

    var $secEnroll = $pageDq.find('#enroll');
    $secEnroll.find('.page-dq__enroll__item').each(function () {
      $(this).attr({
        'data-aos': 'flip-left',
        'data-aos-easing': easing,
        'data-aos-duration': duration,
        'data-aos-delay': delayStart + 100 * i++,
      });
    });

    i = 0;
    // only first 5
    $pageDq.find('#faq .ts-faq').slice(0,5).each(function () {
      $(this).attr({
        'data-aos': 'flip-up',
        'data-aos-easing': easing,
        'data-aos-duration': duration,
        'data-aos-delay': delayStart + 100 * i++,
        'data-aos-once': true,
      });
    });
  }

  // Page Family Link
  // ----------------------------------------
  var $pageFamilyLink = $('.page-family');
  if($pageFamilyLink.length > 0) {

    $pageFamilyLink.find('#dq8').find('h2').attr(animations.fade_up).siblings('img').attr(animations.fade_in);

    var $secServiceList = $pageFamilyLink.find('#service-list');
    $secServiceList.find('h2').attr(animations.fade_up);
    $secServiceList.find('.phone').attr({
      'data-aos': 'fade-down',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });

    var $secBenefits = $pageFamilyLink.find('#benefit');
    $secBenefits.find('h2').attr({
      'data-aos': 'fade-up',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });

    $pageFamilyLink.find('.right-img').attr({
      'data-aos': 'fade-right',
      'data-aos-easing': easing,
      'data-aos-duration': duration,
      'data-aos-delay': delayStart + delayStep,
    });

    // $pageFamilyLink.find('#packages h2').attr(animations.zoom_in);

    i = 0;
    // only first 5
    $pageFamilyLink.find('#faq .ts-faq').slice(0,5).each(function () {
      $(this).attr({
        'data-aos': 'flip-up',
        'data-aos-easing': easing,
        'data-aos-duration': duration,
        'data-aos-delay': delayStart + 100 * i++,
        'data-aos-once': true,
      });
    });
  }

  AOS.init({
    duration,
  });

});

