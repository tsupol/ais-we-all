

<!DOCTYPE html>
<html lang="en">
<head>
  <script src="//assets.adobedtm.com/75133ce79ba1f2c516f7d5f09b683f779a12ac39/satelliteLib-2756bab2480cb6c2674fb064e707912a7e9432b2.js"></script>
  <script src="//www.ais.co.th/base_interface_v2/js/jquery-3.2.1.min.js"></script>
  <script src="//www.ais.co.th/base_interface/js/jquery.typeahead.min.js"></script>
  <script src="//www.ais.co.th/base_interface/js/menutypeahead.js"></script>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Welcome to AIS - The mobile operator of Thailand</title>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
  <meta http-equiv="Pragma" content="no-cache"/>
  <meta http-equiv="Expires" content="0"/>
  <meta http-equiv="X-UA-Compatible" content="IE=10,9"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0"/>
  <!-- Html Meta -->
  <meta name="description" content="We are the leading provider of mobile phone network, the mobile operator of Thailand. With the best in services of internet 4G 3G Fibre Super WIFI and WIFI provider, prepaid, postpaid and mobile phone devices. Find out more here.">
  <meta name="keywords" content="ais,4g internet, mobile operator, wifi service, mobile phone, smartphone, iphone, smartphone store, serenade, 4g, internet provider, devices, eservice, home page ais">
  <!-- Facebook Propertie -->
  <meta property="og:title" content="Welcome to AIS - The mobile operator of Thailand"/>
  <meta property="og:description"
        content="We are the leading provider of mobile phone network, the mobile operator of Thailand. With the best in services of internet 4G 3G Fibre Super WIFI and WIFI provider, prepaid, postpaid and mobile phone devices. Find out more here."/>
  <meta property="og:image" content="share.jpg"/>
  <!-- Google Plus -->
  <meta itemprop="name" content="Welcome to AIS - The mobile operator of Thailand">
  <meta itemprop="description"
        content="We are the leading provider of mobile phone network, the mobile operator of Thailand. With the best in services of internet 4G 3G Fibre Super WIFI and WIFI provider, prepaid, postpaid and mobile phone devices. Find out more here.">
  <meta itemprop="image" content="share.jpg">
  <!-- apple itunes application -->
  <meta name="apple-itunes-app" content="app-id=399758084"/>
  <!-- Start SmartBanner configuration -->
  <meta name="smartbanner:title" content="my AIS">
  <meta name="smartbanner:author" content="AIS ให้คุณใช้ชีวิตได้มากกว่า">
  <meta name="smartbanner:price" content="FREE">
  <meta name="smartbanner:price-suffix-google" content=" Download Now">
  <meta name="smartbanner:icon-google" content="https://lh3.googleusercontent.com/zpXlAyDbHvn4Q7WijKM_HPMYTXOOAhVTkKqZQS3K-LX6-NX-QH-ghF4R-dQu2T-fcfls=w300-rw">
  <meta name="smartbanner:button" content="VIEW">
  <meta name="smartbanner:button-url-google" content="https://play.google.com/store/apps/details?id=com.ais.mimo.eservice&hl=en">
  <meta name="smartbanner:enabled-platforms" content="android">
  <meta name="smartbanner:disable-positioning" content="true">
  <meta name="smartbanner:hide-ttl" content="10000">
  <!-- ถ้าต้องการแสดง banner andriod ให้คอมเม้นบรรทัดด้านล่าง -->
  <meta name="smartbanner:enabled-platforms" content="none">
  <!-- End SmartBanner configuration -->
  <!--Include Style Script-->
  <link rel="stylesheet" href="//www.ais.co.th/base_interface_v2/css/navais_v2.css">
  <style>

  </style>

  <link rel="canonical" href="http://www.ais.co.th/AIS_IfWeAllAreNetwork_Homepage_Website/"/>
  <link rel="stylesheet" href="fonts/db-heavent/stylesheet.css" type="text/css" media="all"/>
  <!--<link rel="stylesheet" href="fonts/db-heavent-cond/stylesheet.css" type="text/css" media="all"/>-->
  <link rel="stylesheet" href="fonts/akrobat/stylesheet.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css" type="text/css" media="all"/>
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" type="text/css" media="all"/>
  <link rel="stylesheet" type="text/css" media="all" href="./css/main.css?1560794295"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>


</head>
<body><script id="__bs_script__">//<![CDATA[
  document.write("<script async src='/browser-sync/browser-sync-client.js?v=2.26.7'><\/script>".replace("HOST", location.hostname));
  //]]></script>

<!--ais_topbar-->
<div id="ais_topbar"></div>
<div class="we-all">

  <div class="page-nav page-nav-family-link">
    <ul class="page-nav__top">
      <li class="page-nav-item"><a href="index.html">Home</a></li>
      <!--  <li class="page-nav-item"><a href="todo">อุ่นใจ Cyber</a></li>-->
      <li class="page-nav-item"><a href="DQ.html">DQ</a></li>
      <li class="page-nav-item"><a href="family-link.html">AIS Family Link</a></li>
      <li class="page-nav-item"><a href="secure-net.html">AIS Secure Net</a></li>
      <li class="page-nav-item"><a target="_blank" href="//www.ewastethailand.com">E-Waste</a></li>
    </ul>
    <div class="page-nav__bottom">
      <button class="page-nav-mb-btn" type="button"><span class="text"></span><span class="icon"><img src="./img/down-arrow.png"></span></button>
      <div class="container mb-dropdown">
        <ul class="page-nav__bottom__content">
          <li class="page-nav-item"><a href="#home">AIS Family Link</a></li>
          <li class="page-nav-item"><a href="#service-list">Service Features</a></li>
          <li class="page-nav-item"><a href="#get-ready">Prepare Before Using</a></li>
          <li class="page-nav-item"><a href="#faq">FAQ</a></li>
        </ul>
      </div>
    </div>
  </div>

  <main role="main" class="flex-shrink-0 page-family">

    <!-- Section : Hero-->
    <div id="home">

      <div class="page-family__hero">
        <div class="container">
          <div class="_outer">
            <div class="page-home__hero__flexc">
              <div class="with-shadow" data-aos="fade-in" data-aos-delay="300">
                <img class="logo" src="./img/family-link/logo-ais-n-family.png"/>
              </div>
              <p data-aos="fade-up">
                AIS is partnering with Google in support of <br>
                the Family Link, an application that <br>
                allows family members to be connected, <br>
                enabling parents to better care for <br>
                and more closely safeguard their <br>
                children’s internet and mobile device <br>
                usage in order to strengthen digital <br>
                immunity within the family unit and <br>
                to promote safe, creative internet usage.
              </p>
            </div>
          </div>

        </div>
      </div>

    </div>

    <!-- Section : Service List -->
    <div id="service-list">

      <div class="page-family__service-list">
        <div class="container">
          <h2>Service Features</h2>
          <div class="row">


            <div class="page-family__service-list__service active">
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-1">Manage Screen Time</h3>
                <p>
                  Parents can set time limits and <br>
                  schedules for their child’s phone usage, <br>
                  including setting controls so <br>
                  phones cannot be used during rest times.
                </p>
              </div>
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-1">
                <img src="./img/family-link/phone-1-icon.png">
                <img class="hover" src="./img/family-link/phone-1-icon-hover.png">
              </div>
            </div>
            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-2">
                <img src="./img/family-link/phone-2-icon.png">
                <img class="hover" src="./img/family-link/phone-2-icon-hover.png">
              </div>
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-2">Website Browsing Scheduling</h3>
                <p>
                  Parents can schedule access <br>
                  to various websites and can also <br>
                  restrict access to specific <br>
                  websites and inappropriate content.
                </p>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-3">Approve or deny New Application Installation</h3>
                <p>
                  Parents can allow or block <br>
                  the use of specific applications <br>
                  on their children’s devices, <br>
                  such as games, social networking sites, etc.
                </p>
              </div>
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-3">
                <img src="./img/family-link/phone-3-icon.png">
                <img class="hover" src="./img/family-link/phone-3-icon-hover.png">
              </div>
            </div>
            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-4">
                <img src="./img/family-link/phone-4-icon.png">
                <img class="hover" src="./img/family-link/phone-4-icon-hover.png">
              </div>
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-4">Oversee Phone Usage Report</h3>
                <p>
                  Parents can monitor the use <br>
                  of applications and duration <br>
                  of usage via reports.
                </p>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-5">Oversee Application Installation</h3>
                <p>
                  Parents can allow or block <br>
                  the installation of new <br>
                  applications on their children’s <br>
                  devices. If parents do <br>
                  not allow the app, their child <br>
                  will be unable to install it on their device.
                </p>
              </div>
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-5">
                <img src="./img/family-link/phone-5-icon.png">
                <img class="hover" src="./img/family-link/phone-5-icon-hover.png">
              </div>
            </div>
            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-6">
                <img src="./img/family-link/phone-6-icon.png">
                <img class="hover" src="./img/family-link/phone-6-icon-hover.png">
              </div>
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-6">View Children’s Current Location</h3>
                <p>
                  Parents can keep track of their <br>
                  child’s current location.
                </p>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="page-family__service-list__phone-container">
              <div class="phone">
                <img class="case" src="./img/family-link/phone_nfill_trans.png">
                <div class="screen"></div>
                <div id="phone-slide-1" class="phone-slide active">
                  <div class="screen">
                    <img src="./img/family-link/phone-1.jpg">
                  </div>
                  <div class="popup">
                    <img src="./img/family-link/phone-1-popup.jpg">
                  </div>
                </div>
                <div id="phone-slide-2" class="phone-slide">
                  <div class="screen">
                    <img src="./img/family-link/phone-2.jpg">
                  </div>
                  <div class="popup">
                    <img src="./img/family-link/phone-2-popup.jpg">
                  </div>
                </div>
                <div id="phone-slide-3" class="phone-slide">
                  <div class="screen">
                    <img src="./img/family-link/phone-3.jpg">
                  </div>
                  <div class="popup">
                    <img src="./img/family-link/phone-3-popup.jpg">
                  </div>
                </div>
                <div id="phone-slide-4" class="phone-slide">
                  <div class="screen">
                    <img src="./img/family-link/phone-4.jpg">
                  </div>
                  <div class="popup">
                    <img src="./img/family-link/phone-4-popup.jpg">
                  </div>
                </div>
                <div id="phone-slide-5" class="phone-slide">
                  <div class="screen">
                    <img src="./img/family-link/phone-5.jpg">
                  </div>
                  <div class="popup">
                    <img src="./img/family-link/phone-5-popup.jpg">
                  </div>
                </div>
                <div id="phone-slide-6" class="phone-slide">
                  <div class="screen">
                    <img src="./img/family-link/phone-6.jpg">
                  </div>
                  <div class="popup">
                    <img src="./img/family-link/phone-6-popup.jpg">
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="page-family__service-list__service-mb">

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-1.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>Manage Screen Time</h3>
                <p>
                  Parents can set time limits and schedules for their child’s phone usage, <br class="not-hide">
                  including setting controls so phones cannot be used during rest times.
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-2.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>Approve or deny New Application Installation</h3>
                <p>
                  Parents can allow or block the use of specific applications <br class="not-hide">
                  on their children’s devices, such as games, social networking sites, etc.
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-3.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>Oversee Application Installation</h3>
                <p>
                  Parents can allow or block the installation of new applications <br class="not-hide">
                  on their children’s devices. If parents do not allow the app, <br class="not-hide">
                  their child will be unable to install it on their device.
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-4.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>Website Browsing Scheduling</h3>
                <p>
                  Parents can schedule access <br class="not-hide">
                  to various websites and can also <br class="not-hide">
                  restrict access to specific websites <br class="not-hide">
                  and inappropriate content.
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-5.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>Oversee Phone Usage Report</h3>
                <p>
                  Parents can monitor the use of <br class="not-hide">
                  applications and duration of usage via reports.
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-6.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>View Children’s Current Location</h3>
                <p>
                  Parents can keep track of their <br class="not-hide">
                  child’s current location.
                </p>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>

    <!-- Section : Get Ready -->
    <div id="get-ready">

      <div class="page-family__get-ready">
        <div class="container">
          <h2>How to Prepare Before Using the Program</h2>
          <div class="page-family__get-ready__menu text-medium">
            <button type="button" data-target="#getRdy-1" onclick="showGetRdy(this)" class="active">For Parents</button>
            <button type="button" data-target="#getRdy-2" onclick="showGetRdy(this)">For Children</button>
          </div>
          <div class="page-family__get-ready__content-list">
            <div id="getRdy-1" class="page-family__get-ready__content-list__content active" style="display: block">
              <ul>
                <li>
                  <p>
                    Devices must be equipped with iOS 9 or a later <br>
                    operating system, or Android 4.4 or higher.
                  </p>
                </li>
                <li>
                  <p>
                    Parents must have a Gmail account.
                  </p>
                </li>
                <li>
                  <p>
                    Parents must live in the same country as their children.
                  </p>
                </li>
                <li>
                  <p>
                    Program is suitable for users with internet packages.
                  </p>
                </li>
                <li>
                  <p class="hasImage">
                    Parents then download the Family Link for Parents application.<br>
                    <img src="./img/family-link/google-play.png">
                    <img src="./img/family-link/app-store.png">
                  </p>
                </li>
              </ul>
              <div class="mb-logo">
                <div class="family-logo">
                  <img src="./img/family-link/family-logo.png">
                </div>
                <div class="download-logo">
                  <img src="./img/family-link/google-play.png">
                  <img src="./img/family-link/app-store.png">
                </div>
              </div>
            </div>

            <div id="getRdy-2" class="page-family__get-ready__content-list__content">
              <ul>
                <li>
                  <p>
                    Devices must be equipped with Android 5.1 <br>
                    operating system and above.
                  </p>
                </li>
                <li>
                  <p>
                    Children must have a Gmail account (parents <br>
                    can use the Family link to create a new <br>
                    Gmail account for children under <br>
                    13 years of age. Additionally, parents <br>
                    can also use the Family Link to add oversight <br>
                    for their child’s existing Gmail account as well.)
                  </p>
                </li>
                <li>
                  <p>
                    Program is suitable for users with data packages.
                  </p>
                </li>
                <li>
                  <p class="hasImage">
                    Download the Family Link for Children & Teens application.<br>
                    <img src="./img/family-link/google-play.png">
                  </p>
                </li>
              </ul>
              <div class="mb-logo">
                <div class="family-logo">
                  <img src="./img/family-link/family-logo.png">
                </div>
                <div class="download-logo">
                  <img src="./img/family-link/google-play.png">
                </div>
              </div>
            </div>
            <img class="right-img" src="./img/family-link/phone-family-link.png">
            <div class="clearfix"></div>

          </div>
        </div>
      </div>

    </div>

    <!-- New Section : How to Register -->
    <div id="how-to-reg">
      <div class="page-family__how-to-reg page-family__how-to-reg--en">
        <div class="container">
          <div class="_inner">
            <h2>Register and Begin Using the Application</h2>
            <!-- Step -->
            <div id="how-to-reg-carousel" class="carousel-wrap">
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-1.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>1.To Apply</h3>
                    <p>Parents press *681*, your child’s 10-digit AIS phone number, and then # to subscribe and receive free internet access for the parent’s AIS phone number.</p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-2.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>2.Confirm Registration and Receive the Download Link for the App</h3>
                    <p>
                      <b>Parent’s device</b>: Receive an SMS confirming your registration and receive your free internet privileges along with the download link for the Google Family Link for
                      Parents app.<br class="not-hide"/>
                      <b>Child’s device</b>: Receive an SMS with the download link for the Google Family Link for Children & Teens app.
                    </p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-3.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>3.Install the Application</h3>
                    <p>
                      <b>Parent’s device</b>: Download and install the Google Family Link for Parents app.<br class="not-hide"/>
                      <b>Child’s device</b>: Download and install the Google Family Link for Children & Teens app.
                    </p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-4.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>4.Sign In</h3>
                    <p>
                      <b>Parent’s device</b>: Sign up by logging in with your personal Gmail account and, from there, add the number of children you want to have connected to you via the app. Parent can
                      create new account through the application.<br class="not-hide"/>
                      <b>Child’s device</b>: Sign up using the log-in for your child’s connected email account.
                    </p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-5.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>5.Pair the Accounts</h3>
                    <p>Set up the connection for both the parent’s and child’s devices at the same time and pair them by entering the pairing code displayed on both devices.
                    </p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-6.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>6.Begin Usage </h3>
                    <p><b>Parent’s device</b> : Parents select their child’s name displayed in the app to start using the service and view their child’s device usage.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- New Section : Package -->
    <div id="packages">
      <div class="page-family__package theme--light">
        <div class="container">
          <div class="_inner">
            <h2>Family Link Package</h2>
            <div class="package-content">
              <p class="content-main">
                <span class="color-white">To Apply press</span><span class="asterisk">*</span>681<span class="asterisk">*</span><br/>
                Your child’s 10-digit AIS phone number#
                <img class="img-tel" src="./img/family-link/how-to-register/telephone.png"/>
              </p>
              <p class="content-note">*Receive free internet 50MB per month<br class="show-sm"/> for access to Family Link service and Google Application</p>
            </div>
            <div class="package-link">
              <a target="_blank" href="https://families.google.com/intl/en-GB/familylink/privacy/notice/">Service Terms and Conditions</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : FAQ -->
    <div id="faq">
      <div class="page-dq__faq sec-faq theme--light">
        <div class="container">
          <h2>FAQ</h2>
          <div class="faq-container">


            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>Can two parents use one Family Link account at the same time?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  Yes. The first parent can add a second parent in the same way that they would add <br>
                  new members to the account. Once the new member has been added, they can proceed as follows:
                </p>
                <ul class="number">
                  <li>Select Menu > Family Group > Manage Parental Rights in the top left corner.</li>
                  <li>Select the name of the person you want to add or remove from the parental role.</li>
                  <li>Select Confirm.</li>
                </ul>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>Can the app be used with other networks?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  Yes, it can be used with other networks. You just need an internet signal and a Gmail account.
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>Can children delete the application?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  The application cannot be deleted or uninstalled from their device unless the user deletes the active Gmail account connected to it.
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>How do you start using the application on the child’s device?</p>
              </div>
              <div class="ts-faq__a">
                <ul>
                  <li>
                    For the parent’s device: The Family Link for Parents application must be installed, which can be accessed using a Gmail account. Generally, a person must be 13 years and older to open a Gmail account; therefore, parents can open a Gmail account on behalf of their child using the settings in the application. Once the child has reached 13 years of age, Google will send an email notification to inform the child. Once the set-up has been completed, the application will then pair the two Gmail accounts belonging to the same family.
                  </li>
                  <li>
                    For the child’s device: The Family Link for Children & Teens application must be installed and can then be accessed via the Gmail account that the parent has opened on behalf of their child (with a different account and password from the parent’s) in order to complete the process of linking the accounts.
                  </li>
                </ul>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>Can Family Link be used on iOS devices?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  Parents can use Family Link on both Android and iOS compatible devices. For children, however, the app can only be used on Android devices.
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>If a parent is using multiple devices, will Family Link work with other devices?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  Yes. Parents can access the app on each individual device using the same Gmail account, which they originally completed the set-up.
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>Can the app be used if I change devices?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  Yes, Family Link can continue to be accessed as usual. Parents can access the app on the new device using the same Gmail account with which they originally completed the set-up.
                </p>
              </div>
            </div>

            <!--            <ul class="ts-pagination">-->
            <!--              <li><a>1</a></li>-->
            <!--              <li><a>2</a></li>-->
            <!--            </ul>-->

            <!-- end all question -->
          </div>
        </div>
      </div>
    </div>

  </main>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
  <script>
    $(document).ready(function () {

      // active menu
      $('.page-nav__top>.page-nav-item:eq(2)>a').addClass('active');

      // New Section : How to register
      // ----------------------------------------

      if(mobileAndTabletcheck() && $(document).width() < 576) {

        $('#how-to-reg-carousel').slick({
          dots: true,
          infinite: true,
          arrows: false,
        });
      }

    });

    // hover service
    function serviceToggle(element) {
      var click_parent = $(element).closest('.page-family__service-list__service');
      $('.page-family__service-list__service').not(click_parent).removeClass('active');
      $(click_parent).addClass('active');

      var id = $(element).data('phone-target');
      $('.phone-slide').not($(id)).removeClass('active');
      $(id).addClass('active');
    }

    //slide mb service
    $('.page-family__service-list__service-mb').slick({
      dots: true,
      arrows: false,
      infinite: true,
    });

    function showGetRdy(element) {
      if(!$(element).hasClass('active')) {
        $('.page-family__get-ready__menu button').removeClass('active');
        $(element).addClass('active');

        var target_id = $(element).data('target');
        $('.page-family__get-ready__content-list__content.active').css({
          position: 'absolute',
          transfrom: 'transitionY(-50%)'
        }).removeClass('active').fadeOut(400, function() {
          $(this).css({
            position: 'relative'
          })
        });
        $(target_id).addClass('active').fadeIn();
      }
    }
  </script>

  <div id="back-to-top"></div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="./dist/app.bundle.js?1560794295"></script>
<!-- .we-all -->

<!--aisfooter-->
<div id="aisfooter"></div>
<!--aisfooter-->
<script src="//www.ais.co.th/base_interface_v2/js/jquery.nav.control.js"></script>
<link rel="stylesheet" href="//www.ais.co.th/base_interface_v2/fonts/fonts.css">
<link rel="stylesheet" href="//www.ais.co.th/base_interface/css/jquery.typeahead.css">
<link rel="stylesheet" href="//www.ais.co.th/base_interface/css/menutypeahead.css">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TC3PWS"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-TC3PWS');</script>
<!-- End Google Tag Manager -->
<script type="text/javascript">__th_page = "ais";</script>
<script type="text/javascript" src="//hits.truehits.in.th/data/s0029135.js"></script>
<script type="text/javascript" src="https://lvs.truehits.in.th/datasecure/s0029135.js"></script>
<script>
  $(function() {

    ais_interface_setup('custom-only-slidemenu','home','th','','submenu.html');
    // ais_interface_setup('mini-menu','home','th','','');

    digitalData = {
      page:{
        pageInfo:{
          pageName:"ais:th:home",
          site: "ais",
          language: "th"
        },
        category:{
          pageType:"home",
          primaryCategory:"home",
        }
      },
    };

  });
</script>
<script type="text/javascript">_satellite.pageBottom();</script>
</body>
</html>
