<!-- Popup -->
<div id="popup-subscribe" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-outer">
    <div class="mp-inner">

      <div class="sp-close close-popup">
      </div>

      <div class="scroll-container form-container">
        <h2 class="heading1">ลงทะเบียนแจ้งความสนใจใช้ AIS Secure Net</h2>

        <form id="contact-form" class="ml-form" action="./" method="POST">
          <div class="row">
            <div class="form-item col-md-6">
              <label class="input-label">เบอร์โทร*</label>
              <input class="ml-input number" name="tel" id="tel" type="text" placeholder="เบอร์โทร*">
            </div>
            <div class="form-item col-md-6">
              <label class="input-label">เลือกแพกเกจ*</label>
              <select class="selectric" name="package_type">
                <option value="-1">เลือกแพกเกจ</option>
                <option value="AIS Secure Net">AIS Secure Net</option>
                <option value="AIS Secure Net Kids">AIS Secure Net Kids</option>
                
              </select>
            </div>
            <div class="form-item col-md-12">
              <label class="input-label">เงื่อนไขการให้บริการ</label>
              <p class="condition">
                <b>บริการ "AIS Secure Net Kids" หรือ “AIS Secure Net” สามารถรองรับการใช้บริการ ดังนี้</b><br class="not-hide">
                1.  เป็นผู้ใช้งาน AIS ทั้งระบบรายเดือนและเติมเงิน โดยจดทะเบียนในนามบุคคลธรรมดา<br class="not-hide">
                2.  กดยืนยันยอมรับเงื่อนไขการให้บริการ "AIS Secure Net Kids" หรือ “AIS Secure Net” <br class="not-hide">
                3.  สถานะหมายเลขเปิดใช้งานเป็นปกติ (ไม่อยู่ระหว่างการระงับใช้บริการ)<br class="not-hide">
                4.  ใช้งานบนเครือข่าย AIS 4G ADVANCED และ AIS 3G และเครือข่ายของผู้ให้บริการในต่างประเทศที่ AIS เปิดให้บริการโรมมิ่ง <br class="not-hide">
                <br class="not-hide">

                <b>บริการ "AIS Secure Net Kids" หรือ “AIS Secure Net” ไม่รองรับการใช้บริการ ดังนี้</b> <br class="not-hide">
                1. ใช้บริการอินเทอร์เน็ตบนเครือข่าย WiFi หรือใช้งานบนเครือข่าย NEXT G ดูข้อมูล คลิก<br class="not-hide">
                2. ใช้บริการซิมประเภท MultiSIM, MultiSIM PLUS, SIM2Fly, กลุ่ม Asian SIM (เช่น Myanmar SIM และ Cambodian SIM), และ Traveller SIM<br class="not-hide">
                3.ใช้บริการเครือข่ายเสมือน (VPN) ทุกประเภททั้งในและต่างประเทศ หรือใช้งาน DNS (Domain Name System) นอกจากที่บริษัทกำหนด<br class="not-hide">
                <br class="not-hide">

                <b>เงื่อนไขการให้บริการอื่น ๆ </b><br class="not-hide">
                1. ผู้ใช้บริการต้องเลือกใช้บริการอย่างใดอย่างหนึ่งระหว่าง "AIS Secure Net Kids" หรือ “AIS Secure Net” (1 บริการ /1 หมายเลข)<br class="not-hide">
                2. บริการฯ จะแจ้งเตือนและบล็อกการเข้าใช้งานเว็บไซต์ทันที หากตรวจพบการเข้าถึงเว็บไซต์ หรือลิ้งค์ที่คาดว่ามีความเสี่ยงจากภัยคุกคามทางไซเบอร์ คลิก และ เว็บไซต์ที่ไม่ คลิก ตามฐานข้อมูลในระบบ โดยอำนวยความสะดวกให้กับผู้ใช้งานโดยไม่ต้องตั้งค่าการใช้งานใดๆ<br class="not-hide">
                3. ข้อมูลส่วนบุคคลของคุณ อันได้แก่ หมายเลขโทรศัพท์ ความสัมพันธ์ระหว่างท่านกับสมาชิกตามหัวข้อ 8.3 (ถ้ามี) และ URL ปลายทางที่ท่านเข้าถึง จำเป็นต้องถูกจัดเก็บให้กับระบบปัญญาประดิษฐ์ (AI) ซึ่งได้ตรวจสอบเว็บไซต์ หรือลิ้งค์ที่ท่านมีการเข้าถึง รวมทั้งเปรียบเทียบกับฐานข้อมูลของบริการ เพื่อให้การให้บริการ “AIS Secure Net” เป็นไปได้อย่างสมบูรณ์ ถูกต้องและเพื่อความปลอดภัยในการใช้งานของคุณและสมาชิกที่ร่วมใช้บริการ <br class="not-hide"> 
                4. กรณีใช้งานบริการ “AIS Secure Net” คุณจะสามารถเข้าใช้งานหน้าเว็บไซต์ที่ระบบได้แจ้งเตือน ไปแล้วได้ โดยกดข้ามการบล็อก (Blocking Page) หากคุณมั่นใจในความปลอดภัยของเว็บไซต์นั้นๆ โดยระบบจะไม่สามารถปกป้องท่านได้ หากท่านกดข้ามบล็อกไปแล้ว และบริษัทฯ ไม่ขอรับผิดชอบใดๆ ต่อความเสียหายที่เกิดขึ้นการเข้าถึงเว็บไซต์นั้นๆ<br class="not-hide">
                5. การปกป้องยังมีผลต่อเนื่อง ถึงแม้คุณแชร์อินเทอร์เน็ต AIS 4G ADVANCED และ AIS 3G จากหมายเลขที่สมัครบริการไปยังอุปกรณ์อื่นๆ <br class="not-hide">
                6. บริการจะแสดงรายงานเว็บไซต์ที่ถูกบล็อก ทั้งเว็บไซต์ที่ไม่ปลอดภัย และเว็บไซต์ไม่เหมาะสมของแต่ละบริการ โดยเป็นข้อมูลภาพรวมของผู้ใช้งานในแต่ละกลุ่ม ไม่้พาะเจาะจงไปในแต่ละรายผู้ใช้งาน ตามลิ้งค์ดังต่อไปนี้<br class="not-hide">
                <span class="tab">
                  • รายงานของบริการ “AIS Secure Net Kids” www.ais.co.th/aissecurenet/kids-report <br class="not-hide">
                  • รายงานของบริการ “AIS Secure Net”  www.ais.co.th/aissecurenet/general-report<br class="not-hide">
                </span>
                7. การบริการในเฟสที่ 2 (จะมีการประชาสัมพันธ์ เมื่อเปิดให้บริการอีกครั้ง) โดยคุณจะสามารถจัดการนโยบายการป้องกัน (Security Policy) ได้ด้วยตัวเอง และสามารถเพิ่มสมาชิกที่ต้องการปกป้องได้เพิ่มเติม เช่น บุตรหลาน หรือ ผู้สูงวัย โดยผู้ใช้งานสามารถใช้บริการเหล่านี้ได้ในเฟสที่ 2 ผ่านทางเว็บไซต์ พร้อมกับบริการอื่นๆ เพิ่มเติมดังนี้ <br class="not-hide">
                <span class="tab">
                  • การจัดการ (เพิ่ม/ลด) สมาชิกที่ต้องการการปกป้อง<br class="not-hide">
                  • การจัดการ (เพิ่ม/ลด) เว็บไซต์ที่ต้องการกรองด้วยตนเอง<br class="not-hide">
                  • การเปิด/ปิด การบล็อกเว็บไซต์ที่เป็นอันตราย หรือเว็บไซต์ที่ไม่เหมาะสม <br class="not-hide">
                  • การกำหนดนโยบายการปกป้อง (Security Policy) ให้กับสมาชิกด้วยตนเอง<br class="not-hide">
                  • การใช้งานระบบ Whitelist<br class="not-hide">
                  • การกำหนดเวลาการใช้งานอินเทอร์เน็ตให้กับตนเองและสมาชิก<br class="not-hide">
                  • การดูรายงานการบล๊อกเว็บไซต์แบบรายบุคคล<br class="not-hide">
                  • การรับรายงานการเข้าถึงเว็บไซต์ของคุณและสมาชิก<br class="not-hide">
                </span>

                8. การให้บริการในเฟสที่ 2 จะมีเงื่อนไขการใช้บริการเพิ่มเติมที่ผู้ใช้งานต้องทราบและยอมรับเงื่อนไขการให้บริการ ดังต่อไปนี้  <br class="not-hide">
                  8.1 เมื่อผู้ใช้งานสมัครบริการสำเร็จ หากผู้ใช้งานด้ลงชื่อเข้าใช้งาน (Login) บนเว็บเซอร์วิส  <br class="not-hide">
                  8.2 บริการจะตั้งค่ามาตรฐานให้กับผู้ใช้งาน และใช้งานได้เพียงการบล๊อกกลุ่มเว็บไซต์ที่เป็นอันตรายเท่านั้น  <br class="not-hide">
                  8.3 ผู้ใช้งานสามารถเข้าถึงบริการของเว็บเซอร์วิส โดยการคลิกลิ้งค์ที่ระบบส่งให้ผ่านทาง SMS และเข้าใช้งานโดยการลงชื่อ (Log-in) ด้วยหมายเลขที่สมัครบริการพร้อมกับรหัสผ่าน (Password) ในรูปแบบของรหัส OTP 4 หลัก<br class="not-hide">
                  8.4 ผู้ใช้งานจะถูกแบ่งออกเป็น 2 ระดับ<br class="not-hide">
                  <span class="tab">
                    1. ระดับผู้ดูแล (Master) สามารถเพิ่มสมาชิกได้มากที่สุด จำนวน 5 หมายเลข และ สามารถกำหนดนโยบายการป้องกัน (Security Policy) ได้ตามจำนวนของสมาชิก<br class="not-hide">
                    2. ระดับสมาชิก (Member) จะไม่สามารถกำหนดนโยบายการป้องกัน (Security Policy) เองได้ และต้องรับนโยบายการป้องกัน จากทางผู้ดูแล (Master) เท่านั้น และผู้ใช้งานระดับสมาชิกจะมีผู้ดูแลได้เพียงหมายเลขที่เดียวเท่านั้น<br class="not-hide">
                  </span>
                  8.5 สำหรับท่านที่เป็นผู้ดูแลสมาชิก (Master) หากท่านต้องการเพิ่มสมาชิกให้ได้รับการปกป้อง ท่านจำเป็นต้องได้รับกับรหัส OTP และ การกด “ยอมรับ” เงื่อนไขการใช้บริการกับหมายเลขที่จะเป็นสมาชิก (สมาชิกต้องเป็นเลขหมาย AIS ที่ตรงตามเงื่อนไขที่บริษัทฯกำหนด)<br class="not-hide">
                  8.6 สำหรับสมาชิก (Member) ที่ได้รับการกำหนดนโยบายการป้องกัน (Security Policy) จากทางผู้ดูแล (Master) จำเป็นต้องเปิดเผยรหัส OTP แก่ผู้ดูแล และต้องกด “ยอมรับ” เงื่อนไขการใช้บริการ เพื่อดำเนินให้การสมัครบริการและการผูกความสัมพันธ์กับหมายเลขที่เป็นผู้ดูแลเสร็จสมบูรณ์<br class="not-hide">
                  8.7 เพื่อการปกป้องอย่างสมบูรณ์ ท่านที่เป็นสมาชิก (Member) จะถูกกำหนดนโยบายการป้องกัน (Security Policy) จากทางผู้ดูแล ท่านไม่สามารถยกเลิก Policy ได้เอง และการยกเลิกบริการจำเป็นต้องได้รับการอนุญาตจากหมายเลขของผู้ดูแล <br class="not-hide">
                  8.8 เฉพาะผู้ใช้งานที่เป็นผู้ดูแลเท่านั้นที่ สามารถเรียกดูรายงานการบล๊อกทั้งเว็บไซต์ที่ไม่ปลอดภัยและเว็บไซต์ไม่เหมาะสมได้ผ่านทางเว็บไซต์<br class="not-hide">
                9. บริษัทฯ ขอสงวนสิทธิ์ ในการยกเลิกบริการ แก้ไขรูปแบบการให้บริการ ปรับเปลี่ยนเงื่อนไข หรือเพิ่มเติมฟังก์ชันการใช้งานตามที่เห็นสมควร โดยจะแจ้งให้ทราบเมื่อมีการเปลี่ยนแปลง<br class="not-hide">
                10. บริษัทฯ ขอสงวนสิทธิ์ การปฏิเสธการให้บริการดังกล่าวนี้แก่ผู้ใช้บริการที่ไม่มีคุณสมบัติตามที่บริษัทกำหนด ตลอดจนสงวนสิทธิ์ ในการแก้ไข เพิ่มเติม หรือยกเลิกเงื่อนไขใด ๆ โดยไม่จำต้องแจ้งให้ทราบล่วงหน้า<br class="not-hide">
                11. บริษัทขอสงวนสิทธิ์ จะระงับหรือยกเลิกการให้บริการ หากบริษัทฯ พิจารณาเห็นว่ามีการนำบริการไปกล่าวอ้าง ดัดแปลง นำไปใช้เพื่อการค้าและแสวงหาผลกำไร หรือนำไปให้บริการต่อกับบุคคลอื่นๆ โดยไม่ได้รับอนุญาต และบริษัทจะดำเนินคดีตามกฏหมาย<br class="not-hide">
                12. บริษัทขอสงวนสิทธิ์ จะระงับหรือยกเลิกการให้บริการหากบริษัทฯ พิจารณาเห็นว่านำบริการ ไปใช้ในทางที่ผิดกฎหมายหรือกระทำการใดที่ผิดกฎหมาย 
                13. กรณีที่ผู้สมัคร ลงทะเบียนเป็นผู้ร่วมทดสอบบริการ ผ่านหน้าเว็บไซต์ www.ais.co.th/aissecurenet/  บริษัทขอสงวนสิทธิ์ ไม่รับผิดชอบต่อความเสียหายอันเกิดจากการนำหมายเลขผู้หนึ่งผู้ใดที่ไม่ต้องการทดสอบบริการจริง มาลงทะเบียน หรือมีการกลั้นแกล้งนำหมายเลขผู้อื่นมาลงทะเบียนแทนด้วยเจตนาหรือไม่ก็ตาม
              </p>
            </div>
            <div class="form-item col-md-12">
              <label class="form-item-checkbox">
                ข้าพเจ้ายอมรับข้อกำหนดและเงื่อนไขการใช้บริการข้างต้น
                <input type="checkbox" name="consent" value="1">
                <span class="checkmark"></span>
              </label>
            </div>
          </div>

          <div class="col-24 popup-footer">
            <button type="submit" class="btn btn-dark">
              ลงทะเบียน
            </button>
          </div>

        </form>

      </div>
      
      <div class="scroll-container thankyou-container" style="display: none;">
        <div class="img-container">
          <img src="./img/secure-net/ty-popup.png">
        </div>
        <h2 class="heading1">ขอบคุณที่สนใจ ร่วมเป็นหนึ่งในผู้ทดสอบบริการกับเรา</h2>
        <p>ระบบจะดำเนินการสมัครบริการให้ภายใน เดือนกรกฏาคม 2562 พร้อมส่ง SMS ยืนยันการสมัครบริการ</p>
      </div>

    </div>
  </div>

</div>


<script src="https://cdn.jsdelivr.net/npm/selectric@1.13.0/src/jquery.selectric.js"></script>
<script>
  $(document).ready(function () {
    $('.selectric').selectric({
      arrowButtonMarkup: '<b class="button"><img src="./img/drop-arrow-black.png"></b>',
    });

    $.validator.addMethod("notEqual", function(value, element, param) {
      return this.optional(element) || value != param;
    }, "*กรุณาเลือกแพกเกจ");

    $.fn.inputFilter = function(inputFilter) {
      return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
      });
    };
    $(".ml-input.number").inputFilter(function(value) {
      return /^-?\d*$/.test(value) && value.length <= 10;
    });
    
    contactFormInit();
    function contactFormInit () {
      var request;
      var validator = $('#contact-form').validate({
        rules: {
          tel: { required: true, number: true, minlength: 9 },
          package_type: { required: true, notEqual: "-1" },
          consent: { required: true },
        },
        messages: {
          tel: { required: '*กรุณากรอกเบอร์โทร', number: '*กรุณากรอกเฉพาะตัวเลข', minlength: '*กรุณากรอกอย่างน้อย 9 ตัว' },
          package_type: { required: '*กรุณาเลือกแพกเกจ' },
        },

        errorPlacement: function(error, element) {
          if (element.hasClass('selectric')) {
            $(element).closest(".selectric-selectric").find('div.selectric').addClass('error');
            error.appendTo( element.closest(".selectric-selectric") );
          }
          else if (element.prop('type') == 'checkbox') {
            $(element).closest(".form-item-checkbox").addClass('error-checkbox');
          }
          else {
            error.appendTo( element.parent() );
            return true
          }
        },

        // send email
        submitHandler: function (form) {
          // Abort any pending request
          if (request) {
            request.abort();
          }
          // setup some local variables
          var $form = $(form);
          $('#popup-contact').addClass('sending');

          // Let's select and cache all the fields
          var $inputs = $form.find("input, select, button, textarea");
          var $selects = $form.find("select");

          // Serialize the data in the form
          var serializedData = $form.serialize();
          
          $inputs.prop("disabled", true);
          $selects.prop("disabled", true).selectric('refresh')

          // Fire off the request to /gmail.php
          request = $.ajax({
            url: "http://demo.rabbitstale.com:3000/subscribe",
            type: "post",
            data: serializedData,
            timeout: 15000
          });

          // Callback handler that will be called on success
          request.done(function (response, textStatus, jqXHR) {
            if(response.status) {
              $('.ml-input').val('');
              $selects.prop('selectedIndex', 0).selectric('refresh');
              $('.form-item-checkbox input').prop('checked', false);

              $('.form-container').hide();
              $('.thankyou-container').removeAttr('style');
            }
            else {
              alert("Something went wrong. Please try again in 10 minutes.");
            }
          });

          // Callback handler that will be called on failure
          request.fail(function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong. Please try again in 10 minutes.");
            // Log the error to the console
            console.error(
              "The following error occurred: " +
              textStatus, errorThrown
            );
          });

          // Callback handler that will be called regardless
          // if the request failed or succeeded
          request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
            $selects.prop("disabled", false).selectric('refresh')
          });
        }
      });

      $('.selectric').on('change', function() {
        $(this).removeClass('error')
        $(this).closest(".selectric-selectric").find('div.selectric').removeClass('error')
        $(this).closest(".selectric-selectric").find('label.error').remove()
      })

      $('.form-item-checkbox input').on('change', function() {
        if($(this).prop('checked')) {
          $(this).removeClass('error')
          $(this).closest('.form-item-checkbox').removeClass('error-checkbox')
        }
      })
      
      $('.close-popup').on('click', function() {
        $('.thankyou-container').hide();
        $('.form-container').show();
      })

      $('#clear-btn').click(function () {
        $('.ml-input').val('').parent().removeClass('has-value');
        validator.resetForm();
      })
    }
  });
</script>
