<!-- Popup -->
<div id="popup-dq" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-outer">
    <div class="mp-inner">

      <div class="sp-close close-popup">
        <!--          <span>×</span>-->
      </div>

      <!-- Thank you message -->
<!--      <div class="_thank-you text-center">-->
<!--        <img class="" src="--><?php //echo get_template_directory_uri() . 'img/thankful.png' ?><!--"/>-->
<!--        <h3>ขอบคุณที่สนใจรถของเรานะคะ</h3>-->
<!--        <p>ทางเราจะรีบติดต่อกลับไปอย่างเร็วที่สุดค่ะ</p>-->
<!--      </div>-->

      <div class="scroll-container">
        <h2 class="heading1">ลงทะเบียนเข้าร่วมทดสอบ DQ และเป็นเครือข่ายกับเราฟรี!</h2>

        <form id="contact-form" class="ml-form" action="./" method="POST">
          <div class="row">
            <div class="form-item col-md-6">
              <label class="input-label">ชื่อ*</label>
              <input class="ml-input" name="first_name" id="first_name" type="text" placeholder="">
            </div>
            <div class="form-item col-md-6">
              <label class="input-label">นามสกุล*</label>
              <input class="ml-input" name="last_name" id="last_name" type="text" placeholder="">
            </div>
            <div class="form-item col-md-12">
              <label class="input-label">ตำแหน่ง*</label>
              <input class="ml-input" name="position" id="position" type="text" placeholder="">
            </div>
            <div class="form-item col-md-12">
              <label class="input-label">โรงเรียน*</label>
              <input class="ml-input" name="school" id="school" type="text" placeholder="">
            </div>
            <div class="form-item col-md-6">
              <label class="input-label">อีเมล*</label>
              <input class="ml-input" name="email" id="email" type="email" placeholder="">
            </div>
            <div class="form-item col-md-6">
              <label class="input-label">เบอร์โทรศัพท์ติดต่อ*</label>
              <input class="ml-input" name="tel" id="tel" type="text" placeholder="">
            </div>
          </div>

          <!-- button -->
          <div class="col-24">
            <button type="submit" class="btn btn-dark">
              เริ่มทำแบบทดสอบ
            </button>
          </div>

        </form>

      </div>

    </div>
  </div>

</div>

<script>
  $(document).ready(function () {

    contactFormInit();
    function contactFormInit () {
      var request;
      var validator = $('#contact-form').validate({
        rules: {
          first_name: { required: true },
          last_name: { required: true },
          email: {
            required: true,
            email: true,
          },
          position: { required: true },
          school: { required: true },
          tel: { required: true },
        },
        messages: {
          name: { required: 'This field is required' },
          email: { required: 'This field is required' },
          tel: { required: 'This field is required' },
        },
        // send email
        submitHandler: function (form) {
          // Abort any pending request
          if (request) {
            request.abort();
          }
          // setup some local variables
          var $form = $(form);
          $('#popup-contact').addClass('sending');

          // Let's select and cache all the fields
          var $inputs = $form.find("input, select, button, textarea");

          // Serialize the data in the form
          var serializedData = $form.serialize();

          $inputs.prop("disabled", true);

          // Fire off the request to /gmail.php
          request = $.ajax({
            url: "./gmail.php",
            type: "post",
            data: serializedData,
            timeout: 15000
          });

          // Callback handler that will be called on success
          request.done(function (response, textStatus, jqXHR) {
            $('.ml-input').val('');
            window.location.href = "https://www.dqtest.org/lang:th/test";
          });

          // Callback handler that will be called on failure
          request.fail(function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong. Please try again in 10 minutes.");
            // Log the error to the console
            console.error(
              "The following error occurred: " +
              textStatus, errorThrown
            );
          });

          // Callback handler that will be called regardless
          // if the request failed or succeeded
          request.always(function () {
            // Reenable the inputs
            $inputs.prop("disabled", false);
          });
        }
      });
      $('#clear-btn').click(function () {
        $('.ml-input').val('').parent().removeClass('has-value');
        validator.resetForm();
      })
    }
  });
</script>
