<!-- Popup -->
<div id="popup-contact" class="ml-popup">
  <div class="back-drop close-popup"></div>
  <div class="mp-outer">
    <div class="mp-inner">

      <div class="sp-close close-popup">
        <!--          <span>×</span>-->
      </div>

      <!-- Thank you message -->
      <div class="_thank-you text-center">
        <img class="" src="<?php echo get_template_directory_uri() . 'img/thankful.png' ?>"/>
        <h3>ขอบคุณที่สนใจรถของเรานะคะ</h3>
        <p>ทางเราจะรีบติดต่อกลับไปอย่างเร็วที่สุดค่ะ</p>
      </div>

      <div class="scroll-container">
        <h2 class="heading1 text-center">สนใจรถรุ่นนี้</h2>
        <h3 class="heading2 text-center color-1">CB150R</h3>

        <!-- Form -->
        <div class="form-wrap">
          <form id="contact-form" class="ml-form" action="./" method="POST">
            <div class="col-24">
              <div class="form-item">
                <input class="ml-input" name="name" id="name" type="text" placeholder="ชื่อ-นามสกุล">
              </div>
            </div>
            <div class="col-24">
              <div class="form-item">
                <input class="ml-input" name="tel" id="tel" type="text" placeholder="เบอร์ติดต่อ">
              </div>
            </div>
            <div class="col-24">
              <div class="form-item">
                <input class="ml-input" name="email" id="email" type="email" placeholder="E-mail">
              </div>
            </div>
            <div class="col-24">
              <div class="form-item">
                <input class="ml-input" name="province" id="province" type="text" placeholder="จังหวัด">
              </div>
            </div>
            <div class="col-24">
              <div class="form-item">
                <input class="ml-input" name="district" id="district" type="text" placeholder="เขต/อำเภอ">
              </div>
            </div>
            <input name="model" id="model" type="hidden">

            <!-- button -->
            <div class="col-24">
              <button type="submit" class="ts-btn _submit-btn btn-primary">
<!--                <div class="_sending">SENDING...</div>-->
                <div class="_send"><span>ส่ง</span></div>
              </button>
            </div>

          </form>
        </div>

      </div>

    </div>
  </div>

</div>
