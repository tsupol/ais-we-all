<?php

/**
 * For images and other assets
 */
function get_template_directory_uri() {
  global $asset_path;
  return $asset_path;
}

/**
 * I will just ignore $post_type
 */
function get_template_part($path, $post_type = '') {
  include($path . '.php');
}

/**
 * Customize -> Site Identity in admin
 */
function the_custom_logo() {
  ?>
  <a href="/" class="custom-logo-link">
    <img class="custom-logo" src="<?php echo get_template_directory_uri() . '/img/logo.png' ?>"/>
  </a>
  <?php
}

