<?php
include_once('ais-header.php');
?>

  <div id="ais_topbar"></div>

  <main role="main" class="flex-shrink-0">
    <div class="container">
      <div class="theme--dark">
        test
        <div class="theme--light">
          light in dark
        </div>
      </div>
      <div class="theme--light">
        test light
      </div>
      <h1 class="mt-5">AIS Secure Net</h1>
      <a class="btn btn-heavy-primary" data-toggle="modal" data-target="#exampleModal">อุ่นใจ Cyber</a>
      <form>
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>
        <div class="form-check">
          <input type="checkbox" class="form-check-input" id="exampleCheck1">
          <label class="form-check-label" for="exampleCheck1">Check me out</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button class="btn btn-outline-primary">Submit</button>
      </form>
    </div>
  </main>

<?php include_once('ais-footer.php');
