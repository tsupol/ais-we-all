<link rel="canonical" href="http://www.ais.co.th/AIS_IfWeAllAreNetwork_Homepage_Website/"/>
<link rel="stylesheet" href="fonts/db-heavent/stylesheet.css" type="text/css" media="all"/>
<!--<link rel="stylesheet" href="fonts/db-heavent-cond/stylesheet.css" type="text/css" media="all"/>-->
<link rel="stylesheet" href="fonts/akrobat/stylesheet.css" type="text/css" media="all"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" type="text/css" media="all"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css" type="text/css" media="all"/>
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" type="text/css" media="all"/>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>
 
