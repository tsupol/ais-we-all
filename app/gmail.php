<?php
require_once "libs/phpmailer/PHPMailerAutoload.php";

function url_origin($s, $use_forwarded_host = false) {
  $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on');
  $sp = strtolower($s['SERVER_PROTOCOL']);
  $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
  $port = $s['SERVER_PORT'];
  $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
  $host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
  $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
  if(!$protocol) $protocol = 'http';
  return $protocol . '://' . $host;
}

function full_url($s, $use_forwarded_host = false) {
  return url_origin($s, $use_forwarded_host) . $s['REQUEST_URI'];
}

// Begin Gmail
// ----------------------------------------
$absolute_url = str_replace('/gmail.php', '', full_url($_SERVER));

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
$mail->SMTPAuth = true;
$mail->Username = "dev.artplore@gmail.com"; // sender email here
$mail->Password = "artplore1234"; //  password here
//Set who the message is to be sent from
// $mail->setFrom('ais@gmail.com', 'AP Honda'); // todo - ask for it
//Set an alternative reply-to address
// $mail->addReplyTo('replyto@gmail.com', 'Name Here');
// $mail->addAddress('sendto@scg.com', 'Name Here');
$mail->addAddress('ton.supol@gmail.com'); // todo - send to client only
//Set the subject line

$mail->Subject = 'AP Honda Motor Show Contact';

// Parsing Variables
// ----------------------------------------
$first_name = $_POST['first_name'] ? $_POST['first_name'] : '-';
$last_name = $_POST['last_name'] ? $_POST['last_name'] : '-';
$position = $_POST['position'] ? $_POST['position'] : '-';
$school = $_POST['school'] ? $_POST['school'] : '-';
$email = $_POST['email'] ? $_POST['email'] : '-';
$tel = $_POST['tel'] ? $_POST['tel'] : '-';

// Simple Body
$mail->Body =
  "First Name: {$first_name}<br/>".
  "Last Name: {$last_name}<br/>".
  "Position: {$position}<br/>".
  "School: {$school}<br/>".
  "Email: {$email}<br/>".
  "Tel: {$tel}<br/>"
;

// PHP Generated Body
ob_start();
// Template body
// require_once "./email-template.php";
// $mail->Body = ob_get_clean();

// Alt Plain Text Body
$mail->AltBody = "first_name: {$first_name}, first_name: {$first_name}, position: {$position}, school_name: {$school}, email: {$email}, tel: {$tel}";

if (!$mail->send()) {
  error($mail->ErrorInfo);
} else {
  success('success');
}

function error($message) {
  header('HTTP/1.1 500 Internal Server Error');
  header('Content-Type: application/json; charset=UTF-8');
  die(json_encode(array('message' => $message, 'code' => 500)));
}

function success($message) {
  header('Content-Type: application/json');
  print json_encode(['message' => $message]);
}
