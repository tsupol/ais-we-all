<?php
$asset_path = "./";

require_once('dev-inc/mimic-wordpress.php'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <script src="//assets.adobedtm.com/75133ce79ba1f2c516f7d5f09b683f779a12ac39/satelliteLib-2756bab2480cb6c2674fb064e707912a7e9432b2.js"></script>
  <script src="//www.ais.co.th/base_interface_v2/js/jquery-3.2.1.min.js"></script>
  <script src="//www.ais.co.th/base_interface/js/jquery.typeahead.min.js"></script>
  <script src="//www.ais.co.th/base_interface/js/menutypeahead.js"></script>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="shortcut icon" href="./favicon.ico" />
  <title>AIS ให้คุณใช้ชีวิตได้มากกว่า ผู้นำเครือข่ายโทรศัพท์เคลื่อนที่ของประเทศไทย</title>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
  <meta http-equiv="Pragma" content="no-cache"/>
  <meta http-equiv="Expires" content="0"/>
  <meta http-equiv="X-UA-Compatible" content="IE=10,9"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0"/>
  <!-- Html Meta -->
  <meta name="description" content="เอไอเอส ผู้ให้บริการเครือข่ายโทรศัพท์เคลื่อนที่รายเดือนและเติมเงิน รวมทั้งบริการต่างๆเกี่ยวกับอุปกรณ์มือถือ สมาร์ทโฟน iPhone อินเตอร์เน็ต WIFI และ AIS 4G คุณภาพ">
  <meta name="keywords" content="ais, เอไอเอส, มือถือ, เครือข่าย 4g, เครือข่ายอินเตอร์เน็ต, โทรศัพท์เคลื่อนที่, mobile operators thailand, iphone">
  <!-- Facebook Propertie -->
  <meta property="og:title" content="AIS ให้คุณใช้ชีวิตได้มากกว่า ผู้นำเครือข่ายโทรศัพท์เคลื่อนที่ของประเทศไทย"/>
  <meta property="og:description"
        content="เอไอเอส ผู้ให้บริการเครือข่ายโทรศัพท์เคลื่อนที่รายเดือนและเติมเงิน รวมทั้งบริการต่างๆเกี่ยวกับอุปกรณ์มือถือ สมาร์ทโฟน iPhone อินเตอร์เน็ต WIFI และ AIS 4G คุณภาพ"/>
  <meta property="og:image" content="share.jpg"/>
  <!-- Google Plus -->
  <meta itemprop="name" content="AIS ให้คุณใช้ชีวิตได้มากกว่า ผู้นำเครือข่ายโทรศัพท์เคลื่อนที่ของประเทศไทย">
  <meta itemprop="description"
        content="เอไอเอส ผู้ให้บริการเครือข่ายโทรศัพท์เคลื่อนที่รายเดือนและเติมเงิน รวมทั้งบริการต่างๆเกี่ยวกับอุปกรณ์มือถือ สมาร์ทโฟน iPhone อินเตอร์เน็ต WIFI และ AIS 4G คุณภาพ">
  <meta itemprop="image" content="share.jpg">
  <!-- apple itunes application -->
  <meta name="apple-itunes-app" content="app-id=399758084"/>
  <!-- Start SmartBanner configuration -->
  <meta name="smartbanner:title" content="my AIS">
  <meta name="smartbanner:author" content="AIS ให้คุณใช้ชีวิตได้มากกว่า">
  <meta name="smartbanner:price" content="FREE">
  <meta name="smartbanner:price-suffix-google" content=" Download Now">
  <meta name="smartbanner:icon-google" content="https://lh3.googleusercontent.com/zpXlAyDbHvn4Q7WijKM_HPMYTXOOAhVTkKqZQS3K-LX6-NX-QH-ghF4R-dQu2T-fcfls=w300-rw">
  <meta name="smartbanner:button" content="VIEW">
  <meta name="smartbanner:button-url-google" content="https://play.google.com/store/apps/details?id=com.ais.mimo.eservice&hl=en">
  <meta name="smartbanner:enabled-platforms" content="android">
  <meta name="smartbanner:disable-positioning" content="true">
  <meta name="smartbanner:hide-ttl" content="10000">
  <!-- ถ้าต้องการแสดง banner andriod ให้คอมเม้นบรรทัดด้านล่าง -->
  <meta name="smartbanner:enabled-platforms" content="none">
  <!-- End SmartBanner configuration -->
  <!--Include Style Script-->
  <link rel="stylesheet" href="//www.ais.co.th/base_interface_v2/css/navais_v2.css">
  <style>

  </style>

  <?php include_once ('dev-header-part.php'); ?>

</head>
<body>
<!--ais_topbar-->
<div id="ais_topbar"></div>
<div class="we-all">
