<?php
?>
<div id="back-to-top"></div>
</div>
<?php include_once ('dev-footer-part.php') ?>
<!-- .we-all -->

<!--aisfooter-->
<div id="aisfooter"></div>
<!--aisfooter-->
<script src="//www.ais.co.th/base_interface_v2/js/jquery.nav.control.js"></script>
<link rel="stylesheet" href="//www.ais.co.th/base_interface_v2/fonts/fonts.css">
<link rel="stylesheet" href="//www.ais.co.th/base_interface/css/jquery.typeahead.css">
<link rel="stylesheet" href="//www.ais.co.th/base_interface/css/menutypeahead.css">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TC3PWS"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-TC3PWS');</script>
<!-- End Google Tag Manager -->
<script type="text/javascript">__th_page = "ais";</script>
<script type="text/javascript" src="//hits.truehits.in.th/data/s0029135.js"></script>
<script type="text/javascript" src="https://lvs.truehits.in.th/datasecure/s0029135.js"></script>
<script>
  $(function() {

    ais_interface_setup('custom-only-slidemenu','home','th','','submenu.html');
    // ais_interface_setup('mini-menu','home','th','','');

    digitalData = {
      page:{
        pageInfo:{
          pageName:"ais:th:home",
          site: "ais",
          language: "th"
        },
        category:{
          pageType:"home",
          primaryCategory:"home",
        }
      },
    };

  });
</script>
<script type="text/javascript">_satellite.pageBottom();</script>
</body>
</html>
