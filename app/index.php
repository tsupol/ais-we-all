<?php
include_once('ais-header.php');
?>

  <div class="page-nav">
    <?php include_once('dev-main-nav-part.php'); ?>
  </div>

  <main role="main" class="flex-shrink-0 page-home">

    <!-- Section : Hero-->
    <div id="sec-home" class="page-home__hero theme--dark">
      <div class="container">
        <div class="_outer">
          <div class="_inner" data-aos="fade-right">
            <h2>ถ้าเราทุกคนคือเครือข่าย</h2>
            <h1 data-aos="fade-in" data-aos-delay="500" class="text-primary">IF WE ALL ARE A NETWORK</h1>
            <p data-aos="fade-in" data-aos-delay="800">
              เครือข่ายเพื่อคนไทย เราไม่ได้ดำเนินธุรกิจแค่วันนี้<br>
              แต่มุ่งมั่นที่จะทำสิ่งดีดีให้กับสังคมตลอดไป<br>
              <a target="_blank" href="http://sustainability.ais.co.th/en/home">AIS Sustainability</a><br>
            </p>
            <div data-aos="fade-in" data-aos-delay="1200" data-aos-once="true">
              <div class="btn btn-outline-light lity-video" href="//www.youtube.com/watch?v=J7cYcesGLcY&t=1s&autoplay=1" data-lity>
                <span class="with-triangle-right">เล่น</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : Description -->
    <div id="sec-desc" class="page-home__desc">
      <div class="container">
        <h2>ถ้าเราทุกคนคือ<span class="text-primary">เครือข่าย</span></h2>
        <p>
          เอไอเอส เครือข่ายเพื่อคนไทย พร้อมสร้างการเปลี่ยนแปลง<br>
          ปูพื้นฐานและสร้างความตระหนักการใช้อินเทอร์เน็ตอย่างสร้างสรรค์<br>
          และมีประโยชน์ต่อคนไทยทุกวัยด้วย 2 แคมเปญหลัก<br>
        </p>
        <a href="#sec-wellness" class="btn page-home__desc__btn btn-cyber text-right">
          <h4>NETWORK FOR THAIs</h4>
          <h3>อุ่นใจ CYBER</h3>
          <img class="char" src="./img/btn_cyberwellness_char.png">
        </a>
        <a href="#sec-e-waste" class="btn page-home__desc__btn text-right">
          <h3>E-Waste</h3>
          <h4>เครือข่ายกำจัดขยะอิเล็กทรอนิกส์</h4>
        </a>
      </div>
    </div>

    <!-- Section : อุ่นใจ Cyber -->
    <div id="sec-wellness" class="page-home__wellness">
      <div class="container gutter-3">
        <div class="row">
          <div class="col-mda">
            <div class="video-wrap">
              <div class="video video-absolute">
                <iframe type="text/html"
                        src="https://www.youtube.com/embed/u3vTmfw7MV4"
                        frameborder="0" allowfullscreen></iframe>
                <!-- <div class="button-play"></div> -->
              </div>
            </div>
          </div>
          <div class="col-mda">
            <h2>อุ่นใจ Cyber</h2>
            <h3>เครือข่ายป้องกันภัยไซเบอร์</h3>
            <p class="overflow-hidden">
              <span class="line-left"></span>
              เอไอเอสจึงให้ความสำคัญกับการพัฒนาเครือข่าย
              ด้าน อุ่นใจ Cyber และ Online Safety
              เพราะเอไอเอสเชื่อว่านี่คือจุดเริ่มต้นที่สำคัญ
              ที่เอไอเอสจะสร้างการเปลี่ยนแปลงปูพื้นฐาน
              และสร้างความตระหนักถึงการใช้อินเทอร์เน็ต
              อย่างสร้างสรรค์และมีประโยชน์ต่อคนไทยทุกวัย
            </p>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : Network Educator -->
    <div id="sec-educator" class="page-home__educator">
      <div class="container gutter-3">
        <div class="page-home__educator__flexc">
          <div class="left">
            <div class="heading">
              <span>Network<br/></span>
              <div class="overflow-hidden">
                <div class="line"></div>
              </div>
              <span>Educator</span>
            </div>
            <p>
              สร้างภูมิคุ้มกันเปรียบเสมือน<br/>
              การฉีดวัคซีน
            </p>
          </div>
          <div class="right">
            <div class="with-shadow">
              <img class="logo" src="./img/logo-home-educator.png"/>
            </div>
            <p>
              เป็นการโปรแกรมอัจริยะเพื่อวัดภูมิคุ้มกันด้านไซเบอร์<br/>
              และเพิ่มพูนทักษะการใช้ชีวิตดิจิทัลของเด็ก มาสร้างภูมิคุ้มกัน<br/>
              ซึ่งเปรียบเสมือนการฉีดวัคซีนให้คนไทยเตรียมพร้อมรับมือ
            </p>
            <a href="DQ.php" class="btn btn-outline-dark">
              <span>รายละเอียด</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : Network Protector -->
    <div id="sec-security" class="page-home__security">
      <div class="container gutter-3">
        <div>
          <h2>Network</h2>
          <h3>Protector</h3>
          <span class="line"></span>
          <h4>เกราะป้องกันจากภัยไซเบอร์</h4>
        </div>
        <div class="row first-row">
          <div class="ts-col col-mda-5">
            <img data-aos="fade-up" class="content-img" src="./img/secure-1.jpg"/>
          </div>
          <div class="ts-col col-mda-7">
            <img class="logo" src="./img/logo-home-family.png"/>
            <p>
              แอปพลิเคชันที่ให้สมาชิกในครอบครัวได้เชื่อมต่อถึงกัน <br>
              และเสริมสร้างภูมิคุ้มกันในการใช้อินเทอร์เน็ต<br>
              ภายในครอบครัว
            </p>
            <a href="family-link.php" class="btn btn-outline-light">
              <span>รายละเอียด</span>
            </a>
          </div>
        </div>
        <div class="row second-row">
          <div class="ts-col col-mda-7">
            <h5><span class="text-primary">AIS</span> Secure Net</h5>
            <p>
              บริการที่ช่วยปกป้องผู้ใช้งานจากภัยไซเบอร์ แจ้งเตือน บล็อก <br>
              และกรองการเข้าถึงเว็บไซต์ที่ไม่เหมาะสมและสุ่มเสี่ยงพบ<br>
              ไวรัส มัลแวร์ และภัยคุกคามอื่นๆ ผ่านเครือข่าย <br>
              AIS 4G ADVANCED และ AIS 3G โดยไม่ต้องลง<br>
              แอปพลิเคชันใดๆ
            </p>
            <a href="secure-net.php" class="btn btn-outline-light">
              <span>รายละเอียด</span>
            </a>
          </div>
          <div class="ts-col col-mda-5">
            <img data-aos="fade-up" class="content-img" src="./img/secure-2.jpg"/>
          </div>
        </div>
      </div>
    </div>


    <!-- Section : E-Waste -->
    <div id="sec-e-waste" class="page-home__e-waste">
      <div class="container gutter-3">
        <div class="row">
          <div class="col-mda">
            <h2>E-Waste</h2>
            <h3>ขยะอิเล็กทรอนิกส์</h3>
            <div class="overflow-hidden">
              <div class="line"></div>
            </div>
            <h4>
              สิ่งแวดล้อมไทย<br>
              จะดีขึ้นได้ด้วยมือคุณ!
            </h4>
            <p>
              เครือข่ายความร่วมมือของคนไทยเพื่อการจัดการขยะ
              อิเล็กทรอนิกส์อย่างยั่งยืน ด้วยความตั้งใจในการวาง
              รากฐานการกำจัดขยะและการสร้างองค์ความรู้
              ให้สังคมไทย
            </p>
            <a href="https://ewastethailand.com/" target="_blank" class="btn btn-outline-dark">
              <span>รายละเอียด</span>
            </a>
          </div>
          <div class="col-mda">
            <div class="video-wrap">
              <div class="video video-absolute shadow-1">
                <iframe type="text/html"
                      src="https://www.youtube.com/embed/C3abSpR0TVI"
                      frameborder="0" allowfullscreen></iframe>
                <!-- <div class="button-play"></div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </main>

  <script>
    $(document).ready(function () {
      // active menu
      $('.page-nav__top>.page-nav-item:eq(0)>a').addClass('active');
    });
  </script>

<?php include_once('ais-footer.php');
