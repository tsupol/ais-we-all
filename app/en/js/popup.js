import { disableBodyScroll, clearAllBodyScrollLocks } from './bodyScrollLock.es6'

(function ($) {

  var SCROLL_SETTINGS = {
    useBothWheelAxes: false,
    suppressScrollX: true
  };

  // The Demo
  // ----------------------------------------

  $('#show-notification').click(function () {
    openPopupById('popup-image');
  });

  // The Mechanism
  // ----------------------------------------

  // watch
  $('._btn-model-contact').click(function (e) {
    e.preventDefault();
    openContactPopup();
    $('#popup-contact .heading1').html($(this).data('model-title'));
    $('#popup-contact .heading2').html($(this).data('model-name'));
    $('#popup-contact #model').val($(this).data('model-name'));
  });
  $('.close-popup').click(function (e) {
    e.preventDefault();
    document.body.classList.remove('body-no-scroll');
    clearAllBodyScrollLocks();
    // console.log('1', $(this).parents('.ml-popup').length);
    $(this).parents('.ml-popup').first().css({
      display: 'none',
    });
  });


  window.openPopupById = function (modalId) {
    var modal = document.getElementById(modalId);
    if (!modal) {
      console.error('no modal with id: ' + modalId);
      return;
    }
    modal.classList.remove('email-sent');
    modal.style.display = "block";
    document.body.classList.add('body-no-scroll');

    // perfect scrollbar
    // if (modalId === 'popup-contact') {
      new PerfectScrollbar('#' + modalId + ' .scroll-container', SCROLL_SETTINGS);
      new PerfectScrollbar('#' + modalId + ' .condition', SCROLL_SETTINGS);
    // }
    disableBodyScroll(document.querySelector("#" + modalId));
  };

  // openPopupById('popup-dq');

})(jQuery);
