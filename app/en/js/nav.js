import { disableBodyScroll, clearAllBodyScrollLocks } from './bodyScrollLock.es6'
import SlideNav from "./slideNav.es6";

(function ($) {
// Back to top
  $('#back-to-top').click(function () {
    var duration = Math.min($(document).scrollTop() || 500, 2000);
    $('html, body').animate({ scrollTop: 0 }, { duration: parseInt(duration) / 2 });
  });

  $('.page-nav-mb-btn').on('click', function() {
    $(this).parent().find('.mb-dropdown').slideToggle(400, function() {
      if($(this).is(":hidden"))
      {
        $(this).parent().find('.page-nav-mb-btn').removeClass('active');
      }
      else {
        $(this).parent().find('.page-nav-mb-btn').addClass('active');
      }
    });
    // $(this).parent().find('.mb-dropdown').toggleClass('expanded');
  })
  $(window).on('resize', function() {
    if(!window.innerWidth < 799.98) {
      $('.mb-dropdown').removeAttr('style');
    } 
  })

  // Scroll to section
  var nav = new SlideNav({
    activeClass: "active",
    toggleButtonSelector: true,
    changeHash: true,
    customSelector: '.page-nav__bottom a:not([target="_blank"])'
    //nav-icon1
  });

})(jQuery);
