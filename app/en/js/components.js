
$(document).ready(function () {

  // Tabs
  // ----------------------------------------

  $('.ts-tabs button').click(function(){
    var tab_id = $(this).attr('data-tab');

    $('.ts-tabs button').removeClass('active');
    $(tab_id).siblings().removeClass('active');

    $(this).addClass('active');
    $(tab_id).addClass('active');
  });

  // FAQ
  // ----------------------------------------

  $('.ts-faq__q').each(function (index) {
    $(this).prepend('<span class="prefix">' + (index + 1) + '.</span>').append('<div class="toggle-sign"></div>')
  });

  $('.ts-faq').click(function () {
    var me = $(this);
    me.toggleClass('active');
    me.children('.ts-faq__a').slideToggle()
    /* display: none for default hide */
  });

  // Auto Pagination
  var perPage = 5;
  $('.faq-container').each(function () {
    var me = $(this);
    var faqs = me.children('.ts-faq');
    var pageCount = Math.ceil(faqs.length / perPage);
    var $pagination = $('<div class="ts-pagination"></div>');
    var i;
    for( i = 0; i< pageCount; i++) {
      $pagination.append('<a>'+(i+1)+'</a>')
    }
    me.append($pagination);

    // hide other page
    faqs.hide().slice(0,perPage * 1).show();
    $pagination.children().first().addClass('active');
  });

  $('.ts-pagination a').click(function () {
    var me = $(this);
    var faqs = me.parents('.faq-container').first().children('.ts-faq');
    var start = me.index() * perPage;
    me.addClass('active').siblings().removeClass('active');
    faqs.hide().slice(start,start + perPage).show();
  });

  $('.ts-pagination').each(function () {
    var me = $(this);
  });

});
