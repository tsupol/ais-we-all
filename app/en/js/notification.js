(function ($) {
  $(document).ready(function () {

    // The Demo
    // ----------------------------------------

    $('#show-alert').click(function () {

      // Single line example

      // Multi-line example
      showAlert({
        duration: 5000, // time to live
        text: `
          <p><span class="alert-time">09:59 - 10.30</span><span>รายละเอียด</span></p>
          <p><span class="alert-time">09:59 - 10.30</span><span>รายละเอียด</span></p>
          <p><span class="alert-time">09:59 - 10.30</span><span>รายละเอียด</span></p>
          <p><span class="alert-time">09:59 - 10.30</span><span>รายละเอียด</span></p>
        `,
      })

    });

    $('#show-alert-cub').click(function () {
      showAlert({
        duration: 50000, // time to live
        text: 'A.P. Honda มีการแสดงเปิดบูท', // plain text
        title: '09:59 - 10.30', // date-time text or title (optional)
        theme: 'cub',
      })
    });

    // Tab Mechanism
    // ----------------------------------------

    var $container = $('#ts-alert-container');

    function showAlert (args) {

      var transitionTime = 500;
      var timeToLive = args.duration || 8000;
      var textTitle = args.title || '';
      var textDesc = args.text;
      if (textTitle) {
        textTitle = `<span class="alert-time">${textTitle}</span>`;
      }
      if (textDesc) {
        textDesc = `<span>${textDesc}</span>`;
      }

      var $newAlert = $(`<div class="ts-alert ${args.theme && args.theme === 'cub' ? 'theme-cub' : ''}">
    <div class="alert-icon"></div>
    <div class="alert-text">
      <div class="text-inner">
      ${textTitle}
      ${textDesc}
      </div>
    </div>
    <div class="alert-close">
      <div class="close-inner"></div>
    </div>
  </div>`);

      $newAlert.find('.alert-close').click(function () {
        var $alert = $(this).parent();
        $alert.css({
          transition: 'all ' + transitionTime + 'ms',
          opacity: 0,
        });
        setTimeout(function () {
          $alert.remove();
        }, transitionTime)
      });

      $newAlert.css({
        transition: 'all ' + transitionTime + 'ms',
        opacity: 0,
      });

      $container.append($newAlert);

      setTimeout(function () {
        $newAlert.css({
          opacity: 1
        });
      }, 10);

      setTimeout(function () {
        $newAlert.find('.alert-close').trigger('click')
      }, timeToLive)

    }

  });
})(jQuery);
