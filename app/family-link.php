<?php
include_once('ais-header.php');
?>

  <div class="page-nav page-nav-family-link">
    <?php include_once('dev-main-nav-part.php'); ?>
    <div class="page-nav__bottom">
      <button class="page-nav-mb-btn" type="button"><span class="text"></span><span class="icon"><img src="./img/down-arrow.png"></span></button>
      <div class="container mb-dropdown">
        <ul class="page-nav__bottom__content">
          <li class="page-nav-item"><a href="#home">AIS Family Link</a></li>
          <li class="page-nav-item"><a href="#service-list">ลักษณะบริการ</a></li>
          <li class="page-nav-item"><a href="#get-ready">เตรียมพร้อมก่อนเริ่มใช้งาน</a></li>
          <li class="page-nav-item"><a href="#how-to-reg">วิธีการสมัคร</a></li>
          <li class="page-nav-item"><a href="#faq">คำถามที่พบบ่อย</a></li>
        </ul>
      </div>
    </div>
  </div>

  <main role="main" class="flex-shrink-0 page-family">

    <!-- Section : Hero-->
    <div id="home">

      <div class="page-family__hero">
        <div class="container">
          <div class="_outer">
            <div class="page-home__hero__flexc">
              <div class="with-shadow" data-aos="fade-in" data-aos-delay="300">
                <img class="logo" src="./img/family-link/logo-ais-n-family.png"/>
              </div>
              <p data-aos="fade-up">
                AIS ร่วมมือกับ Google ในการสนับสนุน Family Link<br>
                แอปพลิเคชันที่ให้สมาชิกในครอบครัวได้เชื่อมต่อถึงกัน<br>
                ผู้ปกครองสามารถดูแลการใช้อินเทอร์เน็ตและอุปกรณ์<br>
                ของบุตรหลานได้ใกล้ชิดมากยิ่งขึ้น เพื่อเสริมสร้าง<br>
                ภูมิคุ้มกันการใช้งานดิจิทัลในครอบครัวอย่าง<br>
                สร้างสรรค์และปลอดภัย
              </p>
            </div>
          </div>

        </div>
      </div>

    </div>

    <!-- Section : Service List -->
    <div id="service-list">

      <div class="page-family__service-list">
        <div class="container">
          <h2>ลักษณะบริการ</h2>
          <div class="row">

            <?php /* for($i=0; $i < 6; $i++) { ?>
              <div class="page-family__service-list__service<?php echo $i == 0 ? ' active' : '' ?>">
                <div class="page-family__service-list__service__des">
                  <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-<?php echo($i+1) ?>">กำหนดการใช้โทรศัพท์</a>
                  <p>
                    ผู้ปกครองสามารถกำหนดระยะเวลา<br>
                    เล่นโทรศัพท์ของบุตรหลาน รวมถึงตั้ง<br>
                    การควบคุมไม่ให้ใช้งานในเวลาพักผ่อน
                  </p>
                </div>
                <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-1">
                  <img src="./img/family-link/phone-1-icon.png">
                <img class="hover" src="./img/family-link/phone-1-icon-hover.png">
                </div>
              </div>
            <?php } */ ?>

            <div class="page-family__service-list__service active">
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-1">กำหนดการใช้โทรศัพท์</h3>
                <p>
                  ผู้ปกครองสามารถกำหนดระยะเวลา<br>
                  เล่นโทรศัพท์ของบุตรหลาน รวมถึงตั้ง<br>
                  การควบคุมไม่ให้ใช้งานในเวลาพักผ่อน
                </p>
              </div>
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-1">
                <img src="./img/family-link/phone-1-icon.png">
                <img class="hover" src="./img/family-link/phone-1-icon-hover.png">
              </div>
            </div>
            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-2">
                <img src="./img/family-link/phone-2-icon.png">
                <img class="hover" src="./img/family-link/phone-2-icon-hover.png">
              </div>
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-2">กำหนดการเข้าเว็บไซต์</h3>
                <p>
                  ผู้ปกครองสามารถกำหนดการเข้าถึง<br>
                  เว็บไซตต่าง ๆ รวมถึงจำกัดการเข้าใช้งาน<br>
                  เว็บไซต์ที่ไม่เหมาะสม
                </p>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-3">กำหนดการใช้งานแอปพลิเคชัน</h3>
                <p>
                  ผู้ปกครองสามารถอนุญาตหรือบล็อก<br>
                  การใช้งานแอปพลิเคชันบนโทรศัพท์<br>
                  ของบุตรหลาน เช่น เกม, โซเชียล เป็นต้น
                </p>
              </div>
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-3">
                <img src="./img/family-link/phone-3-icon.png">
                <img class="hover" src="./img/family-link/phone-3-icon-hover.png">
              </div>
            </div>
            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-4">
                <img src="./img/family-link/phone-4-icon.png">
                <img class="hover" src="./img/family-link/phone-4-icon-hover.png">
              </div>
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-4">ดูรายงานการใช้โทรศัพท์</h3>
                <p>
                  ผู้ปกครองสามารถเฝ้าดูแลการใช้งานแอปฯ<br>
                  และระยะเวลาที่ใช้งานของบุตรหลาน<br>
                  ได้จากรายงาน
                </p>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-5">ดูแลการติดตั้งแอปพลิเคชันใหม่</h3>
                <p>
                  ผู้ปกครองสามารถอนุญาตหรือบล็อก<br>
                  เมื่อบุตรหลานต้องการติดตั้งแอปพลิเคชันใหม่<br>
                  โดยหากผู้ปกครองไม่อนุญาตแอปพลิเคชันนั้นๆ<br>
                  จะไม่ถูกติดตั้งในอุปกรณ์ของบุตรหลาน
                </p>
              </div>
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-5">
                <img src="./img/family-link/phone-5-icon.png">
                <img class="hover" src="./img/family-link/phone-5-icon-hover.png">
              </div>
            </div>
            <div class="page-family__service-list__service">
              <div class="page-family__service-list__service__icon" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-6">
                <img src="./img/family-link/phone-6-icon.png">
                <img class="hover" src="./img/family-link/phone-6-icon-hover.png">
              </div>
              <div class="page-family__service-list__service__des">
                <h3 href="javascript:void(0);" onmouseover="serviceToggle(this);" data-phone-target="#phone-slide-6">ดูตำแหน่งปัจจุบันของบุตรหลาน</h3>
                <p>
                  ผู้ปกครองสามารถติดตามพิกัดตำแหน่ง<br>
                  ที่บุตรหลานอยู่ ณ ปัจจุบันได้
                </p>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="page-family__service-list__phone-container">
              <div class="phone">
                <img class="case" src="./img/family-link/phone_nfill_trans.png">
                <div class="screen"></div>
                <?php for ($i = 0; $i < 6; $i++) { ?>
                  <div id="phone-slide-<?php echo($i + 1) ?>" class="phone-slide<?php echo $i == 0 ? ' active' : '' ?>">
                    <div class="screen">
                      <img src="./img/family-link/phone-<?php echo($i + 1) ?>.jpg">
                    </div>
                    <div class="popup">
                      <img src="./img/family-link/phone-<?php echo($i + 1) ?>-popup.jpg">
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>

          </div>

          <div class="page-family__service-list__service-mb">

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-1.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>กำหนดการใช้โทรศัพท์</h3>
                <p>
                  ผู้ปกครองสามารถกำหนดระยะเวลาเล่นโทรศัพท์ของ<br class="not-hide">
                  บุตรหลาน รวมถึงตั้งการควบคุมไม่ให้ใช้งานในเวลาพักผ่อน
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-2.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>กำหนดการใช้งานแอปพลิเคชัน</h3>
                <p>
                  ผู้ปกครองสามารถอนุญาตหรือบล็อก<br class="not-hide">
                  การใช้งานแอปพลิเคชันบนโทรศัพท์ของบุตรหลาน เช่น เกม, โซเชียล เป็นต้น
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-3.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>ดูแลการติดตั้งแอปพลิเคชันใหม่</h3>
                <p>
                  ผู้ปกครองสามารถอนุญาตหรือบล็อก<br class="not-hide">
                  เมื่อบุตรหลานต้องการติดตั้งแอปพลิเคชันใหม่<br class="not-hide">
                  โดยหากผู้ปกครองไม่อนุญาตแอปพลิเคชันนั้นๆ<br class="not-hide">
                  จะไม่ถูกติดตั้งในอุปกรณ์ของบุตรหลาน
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-4.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>กำหนดการเข้าเว็บไซต์</h3>
                <p>
                  ผู้ปกครองสามารถกำหนดการเข้าถึง<br class="not-hide">
                  เว็บไซตต่าง ๆ รวมถึงจำกัดการเข้าใช้งาน<br class="not-hide">
                  เว็บไซต์ที่ไม่เหมาะสม
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-5.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>ดูรายงานการใช้โทรศัพท์</h3>
                <p>
                  ผู้ปกครองสามารถเฝ้าดูแลการใช้งานแอปฯ<br class="not-hide">
                  และระยะเวลาที่ใช้งานของบุตรหลาน<br class="not-hide">
                  ได้จากรายงาน
                </p>
              </div>
            </div>

            <div class="page-family__service-list__service-mb__slide">
              <img src="./img/family-link/phone-mb-6.png">
              <div class="page-family__service-list__service-mb__slide__des">
                <h3>ดูตำแหน่งปัจจุบันของบุตรหลาน</h3>
                <p>
                  ผู้ปกครองสามารถติดตามพิกัดตำแหน่ง<br class="not-hide">
                  ที่บุตรหลานอยู่ ณ ปัจจุบันได้
                </p>
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>

    <!-- Section : Get Ready -->
    <div id="get-ready">

      <div class="page-family__get-ready">
        <div class="container">
          <h2>เตรียมพร้อมก่อนเริ่มใช้งาน</h2>
          <div class="page-family__get-ready__menu text-medium">
            <button type="button" data-target="#getRdy-1" onclick="showGetRdy(this)" class="active">สำหรับผู้ปกครอง</button>
            <button type="button" data-target="#getRdy-2" onclick="showGetRdy(this)">สำหรับบุตรหลาน</button>
          </div>
          <div class="page-family__get-ready__content-list">
            <div id="getRdy-1" class="page-family__get-ready__content-list__content active" style="display: block">
              <ul>
                <li>
                  <p>
                    อุปกรณ์ที่ติดตั้งระบบปฏิบัติการ iOS เวอร์ชัน 9 ขึ้นไป หรือ<br>
                    ระบบปฏิบัติการ Android เวอร์ชัน 4.4 ขึ้นไป
                  </p>
                </li>
                <li>
                  <p>
                    ต้องมีบัญชี Gmail
                  </p>
                </li>
                <li>
                  <p>
                    อาศัยอยู่ประเทศเดียวกันกับบุตรหลาน
                  </p>
                </li>
                <li>
                  <p>
                    เหมาะสำหรับผู้ใช้งานที่มีแพ็กเกจอินเทอร์เน็ต
                  </p>
                </li>
                <li>
                  <p class="hasImage">
                    ดาวน์โหลดแอปพลิเคชัน Family Link for Parents<br>
                    <img src="./img/family-link/google-play.png">
                    <img src="./img/family-link/app-store.png">
                  </p>
                </li>
              </ul>
              <div class="mb-logo">
                <div class="family-logo">
                  <img src="./img/family-link/family-logo.png">
                </div>
                <div class="download-logo">
                  <img src="./img/family-link/google-play.png">
                  <img src="./img/family-link/app-store.png">
                </div>
              </div>
            </div>

            <div id="getRdy-2" class="page-family__get-ready__content-list__content">
              <ul>
                <li>
                  <p>
                    อุปกรณ์ที่ติดตั้งระบบปฏิบัติการ Android 5.1 ขึ้นไป
                  </p>
                </li>
                <li>
                  <p>
                    ต้องมีบัญชี Gmail (โดยที่ผู้ปกครองสามารถใช้<br>
                    Family Link สร้างบัญชี Gmail ใหม่สำหรับ<br>
                    บุตรหลานที่มีอายุต่ำกว่า 13 ปี นอกจากนี้ยังสามารถใช้ <br>
                    Family Link เพิ่มการดูแลในบัญชี Gmail ของบุตรหลาน<br>
                    ที่มีอยู่แล้วได้อีกด้วย)
                  </p>
                </li>
                <li>
                  <p>
                    เหมาะสำหรับผู้ใช้งานที่มีแพ็กเกจอินเทอร์เน็ต
                  </p>
                </li>
                <li>
                  <p class="hasImage">
                    ดาวน์โหลดแอปพลิเคชัน Family Link for Children & Teens<br>
                    <img src="./img/family-link/google-play.png">
                  </p>
                </li>
              </ul>
              <div class="mb-logo">
                <div class="family-logo">
                  <img src="./img/family-link/family-logo.png">
                </div>
                <div class="download-logo">
                  <img src="./img/family-link/google-play.png">
                </div>
              </div>
            </div>
            <img class="right-img" src="./img/family-link/phone-family-link.png">
            <div class="clearfix"></div>

          </div>
        </div>
      </div>

    </div>

    <!-- New Section : How to Register -->
    <div id="how-to-reg">
      <div class="page-family__how-to-reg">
        <div class="container">
          <div class="_inner">
            <h2>วิธีการสมัคร</h2>
            <!-- Step -->
            <div id="how-to-reg-carousel" class="carousel-wrap">
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-1.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>1.กดสมัคร</h3>
                    <p>ผู้ปกครองกด *681* เบอร์โทรศัพท์เอไอเอส 10 หลักของบุตรหลาน# โทรออก เพื่อสมัครรับสิทธิ์
                      ฟรีอินเทอร์เน็ตสำหรับเบอร์โทรศัพท์เอไอเอสของผู้ปกครอง</p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-2.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>2.ยืนยันการสมัครและรับลิงก์ดาวน์โหลดแอปฯ</h3>
                    <p>
                      <b>โทรศัพท์ผู้ปกครอง</b> : รับ SMS ยืนยันการสมัครและการรับสิทธิ์ ฟรี ดาต้า พร้อมลิงก์ดาวน์โหลด
                      แอปพลิเคชัน Google Family Link for Parents<br class="not-hide"/>
                      <b>โทรศัพท์บุตรหลาน</b> : รับ SMS พร้อมลิงก์ดาวน์โหลดแอปพลิเคชัน
                      Google Family Link for Children & Teens
                    </p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-3.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>3.ติดตั้งแอปพลิเคชัน</h3>
                    <p>
                      <b>โทรศัพท์ผู้ปกครอง</b> : ดาวน์โหลดและติดตั้งแอปพลิเคชัน Google Family Link for Parents<br class="not-hide"/>
                      <b>โทรศัพท์บุตรหลาน</b> : ดาวน์โหลดและติดตั้งแอปพลิเคชัน Google Family Link for Children & Teens
                    </p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-4.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>4.ลงชื่อเข้าใช้งาน</h3>
                    <p><b>โทรศัพท์ผู้ปกครอง</b> : ลงชื่อเข้าใช้งาน โดย Login ผ่าน Gmail Account ของผู้ปกครอง
                      และเพิ่มจำนวนสมาชิกของบุตรหลานสำหรับเชื่อมต่อ (ในกรณีบุตรหลานไม่มี Gmail Account
                      ส่วนตัว ผู้ปกครองสามารถสร้าง Account ในแอปฯ)<br class="not-hide"/>
                      <b>โทรศัพท์บุตรหลาน</b> : ลงชื่อเข้าใช้งาน โดย Login ผ่าน Gmail Account ของบุตรหลาน </p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-5.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>5.เชื่อมต่อแอปพลิเคชัน</h3>
                    <p>นำโทรศัพท์ผู้ปกครองและบุตรหลานมาตั้งค่าพร้อมกัน เพื่อเชื่อมต่อ โดยกรอก รหัส Pair Code
                      ที่แสดงบนโทรศัพท์ของทั้งคู่
                    </p>
                  </div>
                </div>
              </div>
              <!-- Step -->
              <div class="row row-step">
                <div class="col-12 col-sm-3 col-img">
                  <img src="./img/family-link/how-to-register/reg-step-6.png"/>
                </div>
                <div class="col-12 col-sm-9">
                  <div class="desc-wrap">
                    <h3>6.เริ่มใช้งาน</h3>
                    <p><b>โทรศัพท์ผู้ปกครอง</b> : ผู้ปกครองเลือกชื่อบุตรหลานที่แสดงในแอปพลิเคชัน เพื่อเริ่มการใช้งานและ
                      สามารถดูการใช้งานโทรศัพท์ของบุตรหลาน
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- New Section : Package -->
    <div id="packages">
      <div class="page-family__package theme--light">
        <div class="container">
          <div class="_inner">
            <h2>แพ็กเกจ Family Link</h2>
            <div class="package-content">
              <div class="row">
                <div class="col-lg-6">
                  <div class="type">
                    <h2><span class="color-white">สมัครผ่าน</span>การโทร</h2>
                    <p class="content-main">
                      <span class="color-white">กด</span><span class="asterisk">*</span>681<span class="asterisk">*</span>เบอร์<br/>
                      เอไอเอสของบุตร<br/>หลาน#
                      <img class="img-tel" src="./img/family-link/how-to-register/telephone.png"/>
                    </p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="type">
                    <h2><span class="color-white">สมัคร</span>ออนไลน์</h2>
                    <a href="http://www.ais.co.th/networkforthais/package.html" target="_blank" class="btn btn-white">สมัครผ่านแพ็กเกจออนไลน์</a>
                    <p class="content-mark">
                      คลิกเพื่อกรอกเบอร์เอไอเอสของผู้ปกครอง <br>และเบอร์เอไอเอสของบุตรหลาน ตามขั้นตอน
                    </p>
                  </div>
                </div>
              </div>
              <p class="content-note">
                *ฟรีอินเตอร์เน็ต 50 MB/เดือน<br class="show-sm"/> สำหรับใช้บริการ Family Link และ Google Application<br>
                <span class="color-red">*อุปกรณ์ของบุตรหลานใช้งานได้เฉพาะระบบปฏิบัติการ Android</span>
              </p>
            </div>
            <div class="package-link">
              <a target="_blank" href="https://families.google.com/intl/th_ALL/familylink/privacy/notice">เงื่อนไขการให้บริการ</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : FAQ -->
    <div id="faq">
      <div class="page-dq__faq sec-faq theme--light">
        <div class="container">
          <h2>คำถามที่พบบ่อย</h2>
          <div class="faq-container">

            <?php /* for ($i = 9; $i <= 20; $i++): ?>
              <!-- question -->
              <div class="ts-faq">
                <div class="ts-faq__q">
                  <p>DQ คืออะไร?</p>
                </div>
                <div class="ts-faq__a">
                  <p>
                    DQ หรือ Digital Intelligence Quotient เป็นชุดความรู้ 360 องศา ที่รวบรวมทักษะสำคัญด้านการเข้าสังคม
                    การจัดการอารมณ์ และกระบวนการความคิด เพื่อเตรียมความพร้อมให้เด็กอายุ 8-12 ปี เข้าสู่โลกออนไลน์
                    อย่างมีภูมิความรู้และปฏิภาณไหวพริบ เด็กที่มี DQ ดีจะเป็นเด็กฉลาดคิด รู้จักเลือกดูเลือกเล่นสิ่งที่สนุกสนาน
                    และไม่เป็นอันตราย ใช้อินเตอร์เน็ตอย่างสร้างสรรค์ ระวังตัวจากคนแปลกหน้าและไม่ใช้เวลาอยู่หน้าจอนานเกินไป
                  </p>
                  <p>
                    นอกจากเป็นพลเมืองดีออนไลน์แล้ว DQ ยังกระตุ้นเด็กให้มีความคิดสร้างสรรค์ และใช้อินเตอร์เน็ตเป็นเวทีแสดง
                    ความสามารถและพรสวรรค์ของพวกเขาอีกด้วย
                  </p>
                </div>
              </div>
            <?php endfor; */ ?>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>ใน Google Family Link 1 account ผู้ปกครอง 2 คน สามารถใช้พร้อมกันได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ใช้งานได้ โดยที่ผู้ปกครองคนแรกสามารถเพิ่มผู้ปกครองคนที่สองเข้าไปเหมือนกับที่ต้องการเพิ่มสมาชิกใหม่<br>
                  และเมื่อเพิ่มสมาชิกเรียบร้อยแล้ว สามารถดำเนินการได้ดังนี้
                </p>
                <ul class="number">
                  <li>เลือกเมนู > กลุ่มครอบครัว > จัดการสิทธิ์ของผู้ปกครองที่มุมด้านบนซ้าย</li>
                  <li>เลือกบุคคลที่ต้องการเพิ่มหรือนำออกจากบทบาทผู้ปกครอง</li>
                  <li>เลือกยืนยัน</li>
                </ul>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>เครือข่ายอื่นสามารถใช้งานได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ทุกเครือข่ายสามารถใช้ได้ เพียงมีสัญญาณอินเทอร์เน็ตและมี Gmail Account
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>บุตรหลานสามารถลบแอปพลิเคชันได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ไม่สามารถลบหรือ Uninstall ออกจากอุปกรณ์ได้ นอกจากผู้ใช้งานลบ Gmail Account ที่ใช้งานอยู่นั้นออก
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>เริ่มใช้งานกับอุปกรณ์ของบุตรหลานอย่างไร?</p>
              </div>
              <div class="ts-faq__a">
                <ul>
                  <li>
                    สำหรับอุปกรณ์ของผู้ปกครอง: ต้องลงแอปพลิเคชัน Family Link for liarents โดยสามารถเข้าใช้งานด้วย Gmail Account ซึ่งโดยทั่วไปการเปิด Gmail Account จะทำได้เมื่ออายุ 13 ปีขึ้นไป
                    ดังนั้นผู้ปกครองสามารถเปิด Gmail Account แทนบุตรหลานจากการตั้งค่าในแอปพลิเคชันได้เลย และเมื่อบุตรหลานอายุครบ 13 ปี Google จะมีเมล์แจ้งไปให้บุตรหลานทราบ หลังจากตั้งค่าเสร็จ
                    แอปพลิเคชันจะทำการเชื่อมต่อระหว่าง Gmail Account ทั้ง 2 Account ว่าเป็นครอบครัวเดียวกัน
                  </li>
                  <li>
                    สำหรับอุปกรณ์ของบุตรหลาน: ต้องลง Family Link for Children & Teens โดยสามารถเข้าใช้งานด้วย Gmail Account ที่ผู้ปกครองเปิดให้ (คนละ Account และ Password กับของผู้ปกครอง)
                    เพื่อเชื่อมต่อการตั้งค่าระหว่างกัน
                  </li>
                </ul>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>iOS ใช้งานได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ใช้งานได้ทั้ง Android และ iOS สำหรับอุปกรณ์ของผู้ปกครอง ส่วนอุปกรณ์ของบุตรหลานใช้งานได้เฉพาะระบบปฏิบัติการ Android
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>กรณีผู้ปกครองมีอุปกรณ์หลายเครื่อง จะใช้งานได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ใช้งานได้ โดยที่ผู้ปกครองเข้าใช้งานด้วย Gmail Account ที่ผู้ปกครองใช้สำหรับ Google Family Link บนอุปกรณ์แต่ละเครื่อง
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>กรณีเปลี่ยนอุปกรณ์ จะใช้งานได้หรือไม่?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ใช้งานได้ปกติ โดยที่ผู้ปกครองเข้าใช้งานใหม่ด้วย Gmail Account ที่ผู้ปกครองใช้สำหรับ Google Family Link บนอุปกรณ์ใหม่
                </p>
              </div>
            </div>

            <!-- end all question -->
          </div>
        </div>
      </div>
    </div>

  </main>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
  <script>
    $(document).ready(function () {

      // active menu
      $('.page-nav__top>.page-nav-item:eq(2)>a').addClass('active');

      // New Section : How to register
      // ----------------------------------------

      if(mobileAndTabletcheck() && $(document).width() < 576) {

        $('#how-to-reg-carousel').slick({
          dots: true,
          infinite: true,
          arrows: false,
        });
      }

    });

    // hover service
    function serviceToggle (element) {
      var click_parent = $(element).closest('.page-family__service-list__service');
      $('.page-family__service-list__service').not(click_parent).removeClass('active');
      $(click_parent).addClass('active');

      var id = $(element).data('phone-target');
      $('.phone-slide').not($(id)).removeClass('active');
      $(id).addClass('active');
    }

    //slide mb service
    $('.page-family__service-list__service-mb').slick({
      dots: true,
      arrows: false,
      infinite: true,
    });

    function showGetRdy (element) {
      if (!$(element).hasClass('active')) {
        $('.page-family__get-ready__menu button').removeClass('active');
        $(element).addClass('active');

        var target_id = $(element).data('target');
        $('.page-family__get-ready__content-list__content.active').css({
          position: 'absolute',
          transfrom: 'transitionY(-50%)'
        }).removeClass('active').fadeOut(400, function () {
          $(this).css({
            position: 'relative'
          })
        });
        $(target_id).addClass('active').fadeIn();
      }
    }
  </script>

<?php include_once('ais-footer.php');
