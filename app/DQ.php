<?php
include_once('ais-header.php');
?>

  <div class="page-nav page-nav-dq">
    <?php include_once('dev-main-nav-part.php'); ?>
    <div class="page-nav__bottom">
      <button class="page-nav-mb-btn" type="button"><span class="text"></span><span class="icon"><img src="./img/down-arrow.png"></span></button>
      <div class="container mb-dropdown">
        <ul class="page-nav__bottom__content">
          <li class="page-nav-item"><a href="#what-is-dq">What is <img src="./img/dq/dq-nav-logo.png"/> ?</a></li>
          <li class="page-nav-item"><a href="#dq8">8 ทักษะความฉลาดทางดิจิทัล</a></li>
          <li class="page-nav-item"><a href="#benefit">ประโยชน์ที่จะได้รับ</a></li>
          <li class="page-nav-item"><a href="#enroll">วิธีสมัครเรียน</a></li>
          <li class="page-nav-item"><a href="#test">ทำแบบทดสอบ</a></li>
          <li class="page-nav-item"><a href="#faq">คำถามที่พบบ่อย</a></li>
        </ul>
      </div>
    </div>
  </div>

  <main role="main" class="flex-shrink-0 page-dq">

    <a href="https://www.dqtest.org/lang:th/test" target="_blank" id="btn-test-dq"></a>

    <!-- Section : Hero-->
    <div id="what-is-dq">

      <div class="page-dq__hero theme--light">

        <div class="hero_bg">
          <div class="container">
            <div class="position-container">
              <div class="position-container__content" data-aos="fade-in">
                <div class="_padding-page-nav"></div>
                <h1 class="text-primary">What is DQ ?</h1>
                <p class="shadow-white">
                  ความเป็นอัจฉริยะทางดิจิทัล (Digital Intelligence Quotient: DQ)<br/>
                  คือ ผลรวมระหว่าง ความฉลาดทางสังคมอารมณ์ และความสามารถ<br/>
                  ทางเชาวน์ปัญญาที่ช่วยให้เราสามารถเผชิญหน้ากับความท้าทาย<br/>
                  และความกดดันที่เกิดขึ้นกับการใช้ชีวิตบนโลกไซเบอร์
                </p>
                <div class="_padding-carousel"></div>
              </div>
            </div>
          </div>
        </div>

        <!-- Section : Carousel -->
        <div class="page-dq__carousel theme--light">
          <div class="container">
            <!-- Carousel -->
            <div class="carousel-container">
              <div class="carousel-video-wrap">
                <div class="carousel-wrap carousel-video">
                  <!-- slide 1 Video -->
                  <div class="carousel-item--video">
                    <div class="jetpack-video-wrapper">
                      <iframe src="https://www.youtube.com/embed/EFgeXGqJ7qo?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                              allowfullscreen="" data-crossorigin="y" data-ratio="0.5625" data-width="640" data-height="360"
                              style="display: block; margin: 0px; width: 613px; height: 344.813px;"></iframe>
                    </div>
                  </div>
                  <!-- slide 2 Video -->
                  <div class="carousel-item">
                    <div class="jetpack-video-wrapper">
                      <iframe src="https://www.youtube.com/embed/J7cYcesGLcY?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                              allowfullscreen="" data-crossorigin="y" data-ratio="0.5625" data-width="640" data-height="360"
                              style="display: block; margin: 0px; width: 613px; height: 344.813px;"></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <p>
              DQ (Digital Intelligence Quotient) Program คือเครื่องมือที่ออกแบบเพื่อพัฒนาทักษะและความฉลาดด้านดิจิทัลให้กับเด็กๆ
              วัย 8-12 ปี โดยทีมงานนักวิชาการจาก Nanyang Technological University ประเทศสิงคโปร์ ซึ่งได้รับความเชื่อถือและถูกนำ
              ไปใช้สอนเยาวชนในประเทศต่างๆกว่า 110 ประเทศทั่วโลก
            </p>
          </div>
        </div>
      </div>

    </div>

    <!-- Section : DQ8 -->
    <div id="dq8">
      <div class="page-dq__dq8 theme--light">
        <div class="container">
          <h2>8 ทักษะความฉลาดทางดิจิทัล</h2>
          <img class="dq8-img" src="./img/dq/dq-circle.png"/>
        </div>
      </div>
    </div>

    <!-- Section : Benefits -->
    <div id="benefit">
      <div class="page-dq__benefit theme--light">
        <div class="container">
          <h2>ประโยชน์ที่จะได้รับ</h2>
          <div class="btn-group ts-tabs text-medium" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-outline-tab active" data-tab="#tab-1"><span>สำหรับนักเรียน</span></button>
            <button type="button" class="btn btn-outline-tab" data-tab="#tab-2"><span>สำหรับโรงเรียน</span></button>
            <button type="button" class="btn btn-outline-tab" data-tab="#tab-3"><span>สำหรับผู้ปกครอง</span></button>
          </div>
          <!-- tab student -->
          <div id="tab-1" class="tab-content active">
            <div class="row">
              <div class="col-3 col-md-2">
                <img src="./img/dq/dq-benefit-1.png"/>
              </div>
              <div class="col-9 col-md-10 __desc">
                1.ฝึกนิสัยการใช้อินเทอร์เน็ตและอุปกรณ์สมาร์ทโฟนอย่างสร้างสรรค์ เป็นผู้ใช้งาน ที่ฉลาดใช้ ไม่ติด
                เกมออนไลน์ โซเชียลมีเดีย หรือโทรศัพท์มือถือจนเสียสุขภาพ “Be the master of technology,
                not slave.”
              </div>
            </div>
            <div class="row">
              <div class="col-3 col-md-2">
                <img src="./img/dq/dq-benefit-school-2.png"/>
              </div>
              <div class="col-9 col-md-10 __desc">
                2.เป็นพลเมืองดีออนไลน์ คิดก่อนโพสต์ รู้จักคิดวิเคราะห์ว่าข่าวใดเชื่อถือได้และข่าวไหนเป็นข่าวลือ
                รู้จักป้องกันตัวไม่ให้ถูกหลอกลวงง่าย ทั้งคนแปลกหน้าและซื้อสินค้าออนไลน์
              </div>
            </div>
          </div>
          <!-- tab school -->
          <div id="tab-2" class="tab-content">
            <div class="row">
              <div class="col-3 col-md-2">
                <img src="./img/dq/dq-benefit-school-1.png"/>
              </div>
              <div class="col-9 col-md-10 __desc">
                1.หลักสูตร DQ จะช่วยปลูกจิตสำนึกนักเรียนในการใช้อินเทอร์เน็ตและเข้าสังคมออนไลน์ ฝึกเด็กให้
                มีวินัยและควบคุมตัวเองได้ ช่วยลดปัญหาการกลั่นแกล้งออนไลน์และใช้โซเชียล มีเดียในทางที่ไม่เหมาะสม
                ช่วยให้นักเรียนมีสมาธิในการเรียนดีขึ้นและคุณครูมีเวลาถ่ายทอดวิชาความรู้ได้มากขึ้น
              </div>
            </div>
            <div class="row">
              <div class="col-3 col-md-2">
                <img src="./img/dq/dq-benefit-school-2.png"/>
              </div>
              <div class="col-9 col-md-10 __desc">
                2.DQ เป็นมาตรวัดทักษะและความฉลาดด้านดิจิทัลที่ได้มาตรฐานโลก ได้รับการสนับสนุนจาก WEF,
                OECD, IEEE และกว่า 100 ประเทศทั่วโลก คุณครูสามารถนำผลคะแนน DQ ไปใช้เปรียบเทียบ
                ทักษะด้านดิจิทัลกับนักเรียนโรงเรียนอื่น (คล้ายกับคำแนน IQ)
              </div>
            </div>
          </div>
          <!-- tab parent -->
          <div id="tab-3" class="tab-content">
            <div class="row">
              <div class="col-2">
                <img src="./img/dq/dq-benefit-parent-1.png"/>
              </div>
              <div class="col-10 __desc">
                1.ลดความตึงเครียดจากการดุว่าลูก พอใจที่เห็นลูกมีพัฒนาการการใช้อุปกรณ์สมาร์ทโฟน แท็บเล็ต
                และคอมพิวเตอร์อย่างมีวินัยมากขึ้น (พ่อแม่ต้องคอยส่งเสริมลูกให้ทำกิจกรรมอย่างอื่นด้วย
                เช่น เล่นกีฬา ออกกำลังกาย และกิจกรรมอื่นร่วมกันในครอบครัว ฯลฯ)
              </div>
            </div>
            <div class="row">
              <div class="col-2">
                <img src="./img/dq/dq-benefit-parent-2.png"/>
              </div>
              <div class="col-10 __desc">
                2.ช่วยปูพื้นฐานให้ลูกฝึกทักษะที่สำคัญด้านดิจิทัล (eight-digital skills) รู้จักระมัดระวัง
                คนแปลกหน้าในโลกออนไลน์ รวมถึงการซื้อไอเท็มในเกมและซื้อสินค้าออนไลน์ และรู้ความเหมาะสมในการโพสต์ข้อความหรือรูปภาพในโซเชียลมีเดีย
              </div>
            </div>
            <div class="row">
              <div class="col-2">
                <img src="./img/dq/dq-benefit-parent-3.png"/>
              </div>
              <div class="col-10 __desc">
                3.ช่วยเตรียมความพร้อมให้ลูกเติบโตเป็นผู้ใหญ่ที่มีวินัยและรับผิดชอบในสังคมออนไลน์
                เคารพกฎกติกา มารยาท และกฎหมาย สามารถดูแลตัวเองได้ในโลกออนไลน์
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : Enroll -->
    <div id="enroll">
      <div class="page-dq__enroll theme--light">
        <div class="container">
          <h2>วิธีสมัครเรียนหลักสูตร DQ</h2>
          <div class="row">
            <!-- Item -->
            <div class="col-6 col-md-4">
              <div class="page-dq__enroll__item">
                <img src='./img/dq/step-1.png'/>
                <div class="__content">
                  ไปที่ <a target="_blank" href="https://www.dqworld.net">https://www.dqworld.net</a>
                </div>
              </div>
            </div>
            <!-- Item -->
            <div class="col-6 col-md-4">
              <div class="page-dq__enroll__item">
                <img src='./img/dq/step-2.png'/>
                <div class="__content">
                  ลงทะเบียนด้วย email ผู้ปกครอง
                </div>
              </div>
            </div>
            <!-- Item -->
            <div class="col-6 col-md-4">
              <div class="page-dq__enroll__item">
                <img src='./img/dq/step-3.png'/>
                <div class="__content">
                  ทำแบบทดสอบ DQ Test
                </div>
              </div>
            </div>
            <!-- Item -->
            <div class="col-6 col-md-4">
              <div class="page-dq__enroll__item">
                <img src='./img/dq/step-4.png'/>
                <div class="__content">
                  ผล DQ Test<br/>
                  จะถูกส่งไปที่ email ผู้ปกครอง
                </div>
              </div>
            </div>
            <!-- Item -->
            <div class="col-6 col-md-4">
              <div class="page-dq__enroll__item">
                <img src='./img/dq/step-5.png'/>
                <div class="__content">
                  เรียนหลักสูตร DQ ฟรี!<br/>
                  ที่ <a target="_blank" href="https://www.dqworld.net">https://www.dqworld.net</a><br/>
                  ทำแบบฝึกหัดครบ 8 Zones จึงจบหลักสูตร

                </div>
              </div>
            </div>
            <!-- Item -->
            <div class="col-6 col-md-4">
              <div class="page-dq__enroll__item">
                <img src='./img/dq/step-6.png'/>
                <div class="__content">
                  ผ่านการทดสอบทั้ง 8 ทักษะ<br/>
                  และรับ Certificate<br>
                  จาก DQ World และ AIS
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : Test Invitation -->
    <div id="test">
      <div class="page-dq__test theme--light">
        <div class="container">
          <div class="page-dq__test__inner">
            <h2>เชิญชวนเข้าร่วมทำแบบทดสอบ</h2>
            <p>
              DQ (Digital Intelligence Quotient) Program คือเครื่องมือที่ออกแบบ
              เพื่อพัฒนาทักษะและความฉลาดด้านดิจิทัลให้กับเด็กๆวัย 8-12 ปี
              โดยทีมงานนักวิชาการจาก Nanyang Technological University
              ประเทศสิงคโปร์ ซึ่งได้รับความเชื่อถือและถูกนำไปใช้สอนเยาวชน
              ในประเทศต่างๆกว่า 110 ประเทศทั่วโลก
              <a class="inline-link" href="https://www.dqworld.net">รายละเอียดเพิ่มเติม</a>
            </p>
            <div class="line"></div>
            <div class="row">
              <div class="col col-md-6">
                <h3>สำหรับโรงเรียน</h3>
                <!-- <a onclick="openPopupById('popup-dq')" class="btn btn-outline-dark"><span>ทำแบบทดสอบ</span></a> -->
                <a href="#test" class="btn btn-outline-dark"><span>ทำแบบทดสอบ</span></a>
              </div>
              <div class="col col-md-6">
                <h3>สำหรับบุคคลทั่วไป</h3>
                <a target="_blank" href="https://www.dqtest.org/lang:th/test" class="btn btn-outline-dark"><span>ทำแบบทดสอบ</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Section : FAQ -->
    <div id="faq">
      <div class="page-dq__faq sec-faq theme--light">
        <div class="container">
          <h2>คำถามที่พบบ่อย</h2>
          <div class="faq-container">

            <?php /* for ($i = 9; $i <= 20; $i++): ?>
              <!-- question -->
              <div class="ts-faq">
                <div class="ts-faq__q">
                  <p>DQ คืออะไร?</p>
                </div>
                <div class="ts-faq__a">
                  <p>
                    DQ หรือ Digital Intelligence Quotient เป็นชุดความรู้ 360 องศา ที่รวบรวมทักษะสำคัญด้านการเข้าสังคม
                    การจัดการอารมณ์ และกระบวนการความคิด เพื่อเตรียมความพร้อมให้เด็กอายุ 8-12 ปี เข้าสู่โลกออนไลน์
                    อย่างมีภูมิความรู้และปฏิภาณไหวพริบ เด็กที่มี DQ ดีจะเป็นเด็กฉลาดคิด รู้จักเลือกดูเลือกเล่นสิ่งที่สนุกสนาน
                    และไม่เป็นอันตราย ใช้อินเตอร์เน็ตอย่างสร้างสรรค์ ระวังตัวจากคนแปลกหน้าและไม่ใช้เวลาอยู่หน้าจอนานเกินไป
                  </p>
                  <p>
                    นอกจากเป็นพลเมืองดีออนไลน์แล้ว DQ ยังกระตุ้นเด็กให้มีความคิดสร้างสรรค์ และใช้อินเตอร์เน็ตเป็นเวทีแสดง
                    ความสามารถและพรสวรรค์ของพวกเขาอีกด้วย
                  </p>
                </div>
              </div>
            <?php endfor; */ ?>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>DQ คืออะไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  DQ หรือ Digital Intelligence Quotient เป็นชุดความรู้ 360 องศา ที่รวบรวมทักษะสำคัญด้านการเข้าสังคม
                  การจัดการอารมณ์ และกระบวนการความคิด เพื่อเตรียมความพร้อมให้เด็กอายุ 8-12 ปี เข้าสู่โลกออนไลน์
                  อย่างมีภูมิความรู้และปฏิภาณไหวพริบ เด็กที่มี DQ ดีจะเป็นเด็กฉลาดคิด รู้จักเลือกดูเลือกเล่นสิ่งที่สนุกสนาน
                  และไม่เป็นอันตราย ใช้อินเตอร์เน็ตอย่างสร้างสรรค์ ระวังตัวจากคนแปลกหน้าและไม่ใช้เวลาอยู่หน้าจอนานเกินไป
                </p>
                <p>
                  นอกจากเป็นพลเมืองดีออนไลน์แล้ว DQ ยังกระตุ้นเด็กให้มีความคิดสร้างสรรค์ และใช้อินเตอร์เน็ตเป็นเวทีแสดง
                  ความสามารถและพรสวรรค์ของพวกเขาอีกด้วย
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>Digital Citizenship คืออะไร มีความสำคัญอย่างไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  Digital Citizenship หรือการเป็นพลเมืองดิจิทัลเป็นการสอนให้ผู้ใช้อินเตอร์เน็ตเข้าใจสภาวะของโลกออนไลน์  เข้าใจธรรมชาติของการสื่อสารออนไลน์ ฝึกให้ใช้สื่อดิจิทัลเท่าที่จำเป็น พัฒนาทักษะการคิดวิเคราะห์ข้อมูลและเนื้อหาที่ได้อ่านหรือเห็นออนไลน์  รู้จักแยกแยะว่าเรื่องใดจริงหรือเท็จ ถูกหรือผิด ไม่เชื่อทุกอย่างที่เห็นหรืออ่าน มีมนุษยสัมพันธ์ที่ดีกับเพื่อน ๆ และคนรู้จักในสื่อโซเชียล คิดก่อนโพสต์เพื่อไม่ทำให้ผู้อื่นเดือดร้อน ควบคุมอารมณ์ได้เมื่อถูกรังแกออนไลน์ รู้ถึงอันตรายในโลกออนไลน์และสามารถป้องกันตัวเองได้ สร้างอัตลักษณ์ที่สุจริตและรับผิดชอบในโลกออนไลน์ และไม่ละเมิดสิทธิคนอื่น พูดง่าย ๆ คือสอนให้เป็นพลเมืองดีออนไลน์นั่นเอง
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>หลักสูตร DQ เชื่อถือได้ขนาดไหน?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  หลักสูตร DQ เขียนขึ้นโดยทีมนักวิชาการจากมหาวิทยาลัย Nanyang Technological University ประเทศสิงคโปร์ และ Iowa State University ประเทศสหรัฐอเมริกา ได้รับการสนับสนุนจากสภาเศรษฐกิจโลก (World Economic Forums หรือ WEF) องค์กรเพื่อความร่วมมือและพัฒนาทางเศรษฐกิจ (Organization for Economic Cooperation and Development หรือ OECD) และสถาบันวิชาชีพวิศวกรไฟฟ้าและอิเลคทรอนิกส์ (Institute of Electrical and Electronics Engineers หรือ IEEE) ทำหน้าที่ดูแลเทคโนโลยีเกี่ยวกับไฟฟ้าและคอมพิวเตอร์ โดยได้จัดตั้งเป็นสมาพันธ์ส่งเสริมความฉลาดทางดิจิทัล (Coalition for Digital Intelligence: CDI) ประกอบไปด้วยสมาชิกจากประเทศต่าง ๆ ทั่วโลกโดยมีวัตถุประสงค์เพื่อพัฒนาความฉลาดทางดิจิทัลและวางกรอบการทำงานเพื่อให้ทุกประเทศสามารถนำไปใช้ร่วมกันทั้งรัฐบาล สถานศึกษา และผู้ให้บริการด้านเทคโนโลยี โดยมีสถาบัน DQ ประเทศสิงคโปร์เป็นตัวหลักในการพัฒนาหลักสูตร ปัจจุบันมีกระทรวงศึกษาธิการและสถานศึกษาจากประเทศต่าง ๆ นำ DQ ไปให้ความรู้แก่เด็ก ๆ มากกว่า 100 ประเทศทั่วโลก
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>เด็กๆจะได้ประโยชน์อะไรจากการเรียน DQ?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                    กว่า 100 ประเทศทั่วโลกได้นำหลักสูตร DQ ไปใช้แล้วพบว่า...
                </p>
                <ul class="number">
                  <li>ปลูกฝังทัศนคติเด็กให้ใช้อินเตอร์เน็ตอย่างปลอดภัยและรับผิดชอบ</li>
                  <li>บริหารเวลาหน้าจอได้ดีขึ้น</li>
                  <li>มีความเข้าใจดีขึ้นเรื่องการพบปะผู้คนออนไลน์ สิทธิส่วนตัว และการปกป้องข้อมูลส่วนตัว</li>
                  <li>เพิ่มทักษะการอ่านข้อมูลและการใช้สื่อออนไลน์</li>
                  <li>เป็นพลเมืองดิจิทัล มีความเห็นอกเห็นใจผู้อื่น</li>
                  <li>สร้างสุขภาวะออนไลน์</li>
                  <li>ชักจูงผู้ปกครองให้มีส่วนร่วมดูแลและให้คำแนะนำแก่เด็กในการใช้อินเตอร์เน็ต โรงเรียนมีความเข้าใจและสนใจเด็กเพิ่มมากขึ้นเรื่องการเป็นพลเมืองดิจิทัล</li>
                  <li>มีส่วนช่วยให้เด็กเรียนหนังสืออย่างมีประสิทธิภาพมากขึ้น</li>
                </ul>
                <p>
                  &nbsp;&nbsp;&nbsp;ผลงานวิจัยระบุว่านักเรียนที่เรียนจบบทเรียน DQ จะทำคะแนน DQ ได้ดีขึ้นประมาณ 10% และความเสี่ยงต่ออันตรายในโลกไซเบอร์จะลดลง 15%
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>เด็กจะเรียน DQ ได้ต้องมีคุณสมบัติอย่างไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  - อายุ 8-12 ปี<br class="not-hide">
                  - ดูการ์ตูน เล่นเกม  รือแชทออนไลน์ทุกสัปดาห์โดยใช้แท้ปเล็ต PC หรือโทรศัพท์มือถือ<br class="not-hide">
                  - ผู้ปกครองควรสละเวลาเป็นพี่เลี้ยงลูกขณะทำแบบสอบถามและแบบฝึกหัด บทเรียน DQ แต่ละบทใช้เวลาเฉลี่ย
                    หนึ่งชั่วโมง เด็กที่สมาธิสั้นอาจเลิกเรียนกลางคัน คุณพ่อคุณแม่ควรจัดเวลาให้ลูกเรียนให้จบบทและเรียนให้ครบ
                    ทั้ง 8 บทเรียน
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>เด็กๆต้องเตรียมอุปกรณ์อะไรบ้าง?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ใช้เพียงสองอย่างคือ 1) แท้ปเล็ตขนาด 7 นิ้วขึ้นไป โน้ตบุ๊ค หรือ PC ก็ได้ที่เชื่อมต่อกับอินเตอร์เน็ต และ 2) email ผู้ปกครองสำหรับลงทะเบียน
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>บทเรียน DQ มีเนื้อหาอะไรบ้าง</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  เป้าหมายหลักของ DQ คือให้ความรู้ความเข้าใจแก่เด็กเรื่องการเป็นพลเมืองดิจิทัล (Digital Citizenship) สอนเด็กให้เป็นคนสุจริตคือคิด พูด และทำแต่สิ่งที่ดีออนไลน์ หลักสูตร DQ ประกอบด้วย 8 บทเรียน (Zone) ได้แก่</p>
                <ul class="number">
                  <li><p>Digital Citizen Identity<br class="not-hide">สร้างอัตลักษณ์ที่แท้จริง</p></li>
                  <li><p>Screen-Time Management<br class="not-hide">รู้จักบริหารเวลาหน้าจอ</p></li>
                  <li><p>Cyberbullying Management<br class="not-hide">รู้ว่าควรทำอย่างไรเมื่อถูกรังแก</p></li>
                  <li><p>Cyber Security Management<br class="not-hide">ท่องเน็ตอย่างปลอดภัย</p></li>
                  <li><p>Digital Empathy<br class="not-hide">ใจเขาใจเรา</p></li>
                  <li><p>Digital Footprints<br class="not-hide">โพสต์อะไรไว้ให้รู้ถึงผลที่จะตามมา</p></li>
                  <li><p>Critical Thinking<br class="not-hide">ทักษะในการคิดเป็น</p></li>
                  <li><p>Privacy Management<br class="not-hide">รู้สิทธิส่วนตัว</p></li>
                </ul>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>จำเป็นต้องเรียนให้ครบทั้ง 8 บทเรียนไหม?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ทั้ง 8 บทเรียนเป็นทักษะที่จำเป็นสำหรับเด็ก ๆ เพื่อท่องเน็ตอย่างปลอดภัยและมีเชาวน์ไวไหวพริบ เอไอเอสแนะนำผู้ปกครองให้ชักชวนลูกเรียนทั้ง 8 บทเรียนโดยสามารถเลือกเรียนบทไหนก่อนก็ได้
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>ใช้เวลาเรียนนานไหม?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  เด็กประเทศอื่นใช้เวลาเรียนเฉลี่ยบทละหนึ่งชั่วโมง ทั้งหมดแปดบทเรียนจะใช้เวลาประมาณ 8-10 ชั่วโมง เด็กอายุ 8-10 ปีอาจใช้เวลาอ่านและทำแบบฝึกหัดนานกว่าเด็กอายุ 11-12 ปี
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>ขั้นตอนการสมัครเป็นอย่างไร?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  การฝึกทักษะ DQ มีขั้นตอนง่าย ๆ 6 ขั้นตอน<br class="not-hide">
                </p>
                <ul class="number">
                  <li><p>ล้อคอิน https://www.dqtest.org</p></li>
                  <li><p>ผู้ปกครองช่วยลูกลงทะเบียนโดยใช้ Email ผู้ปกครอง</p></li>
                  <li><p>ให้ลูกทำแบบทดสอบ DQ Test ใช้เวลาประมาณ 30 นาที ผู้ปกครองช่วยเป็นพี่เลี้ยง</p></li>
                  <li><p>เมื่อทำเสร็จจะได้รับผล DQ Test ภายใน 15 นาที ส่งไปที่ Email ผู้ปกครอง</p></li>
                  <li><p>ส่งเสริมทักษะทางดิจิทัลให้ลูกด้วยการสมัครเรียนDQ ฟรี! ที่ https://www.dqworld.net ทำแบบฝึกหัดให้ครบ 8 Zones ใช้เวลาประมาณ 8-10 ชั่วโมงก็จบหลักสูตร</p></li>
                  <li><p>เมื่อเรียนครบ 8 Zones จะได้รับใบคะแนน DQ ทาง Email เด็กที่ได้คะแนนรวมเกิน 100 ถือว่าอยู่ในเกณฑ์ดี จะได้รับ certificate จาก DQ World และ AIS</p></li>
                </ul>
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>เมื่อเด็กเรียนครบ 8 zones แล้วยังไงต่อ?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ผู้ปกครองจะได้รับใบคะแนน (DQ Individual Report) แสดงให้เห็นว่าเด็กมีความเข้าใจทักษะที่จำเป็นทั้งแปดด้านมากน้อยเพียงใด รวมถึงจุดเด่นและจุดอ่อน และคำแนะนำสำหรับพ่อแม่
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>จะมั่นใจได้อย่างไรว่าคำตอบของเด็กรวมถึงข้อมูลส่วนตัวจะไม่ถูกนำไปใช้เพื่อการค้า?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  ในขั้นตอนการสมัครจะมีจดหมายขอคำยินยอมจากผู้ปกครองอนุญาตให้เอไอเอสและมหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี (มจธ.) นำข้อมูลที่ได้จากแบบสอบถามไปใช้เพื่อประโยชน์ในงานวิจัยและส่งเสริมทักษะทางดิจิทัลให้กับเยาวชนไทยเท่านั้น เอไอเอสจะไม่นำข้อมูลที่ได้ไปใช้เชิงธุรกิจโดยเด็ดขาด
                </p>
              </div>
            </div>

            <div class="ts-faq">
              <div class="ts-faq__q">
                <p>ล้อคอิน DQ Test.org แล้วเจอแบบสอบถามเป็นภาษาอังกฤษ?</p>
              </div>
              <div class="ts-faq__a">
                <p>
                  สามารถเลือกแบบสอบถาม (DQ Test) เป็นภาษาไทยได้โดยกดปุ่มภาษาไทยที่มุมบนขวาของจอ
                </p>
              </div>
            </div>
            <!--            <ul class="ts-pagination">-->
            <!--              <li><a>1</a></li>-->
            <!--              <li><a>2</a></li>-->
            <!--            </ul>-->

            <!-- end all question -->
          </div>
        </div>
      </div>
    </div>

  </main>

  <?php include_once('popups/popup-DQ.php') ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
  <script>
    $(document).ready(function () {

      $('.carousel-wrap').slick({
        dots: true,
        infinite: true,
        prevArrow: '<div class="arrow-prev"></div>',
        nextArrow: '<div class="arrow-next"></div>',
      });

      AOS.init();

      // active menu
      $('.page-nav__top>.page-nav-item:eq(1)>a').addClass('active');
    });
  </script>

<?php include_once('ais-footer.php');
