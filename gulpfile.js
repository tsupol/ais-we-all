var gulp = require('gulp');

var minifyCss = require('gulp-minify-css');

var sass = require('gulp-sass');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

var notify = require('gulp-notify');

var browserSync = require('browser-sync');

var reload = browserSync.reload;

var connectPHP = require('gulp-connect-php');

// For ES6 Javascript
var jsSrc = './app/js/index.js';
var jsDist = './app/dist/';
var jsWatch = './app/*.js';
var jsFiles = [jsSrc];
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var glob = require('glob');
// var es = require('event-stream');
var through = require('through2');


// ////////////////////////////////////////////////
// Paths to source files
// paths haves changed a bit.
// ///////////////////////////////////////////////

var paths = {
  html: ['./app/**/*.php'],
  css: ['./app/styles/**/*.scss'],
  script: ['./app/js/**/*.js']
};

gulp.task('mincss', function () {

  var processors = [
    autoprefixer,
    // flexibility // not working properly
  ];

  return gulp.src(paths.css)

    .pipe(sass().on('error', sass.logError))

    .pipe(postcss(processors))

    // .pipe(minifyCss())

    .pipe(gulp.dest('app/css'))

    // .pipe(reload({ stream: true }));
    // .pipe(reload());
    .pipe(browserSync.stream({match: '**/*.css'}));

});


// ////////////////////////////////////////////////
// HTML task
// ///////////////////////////////////////////////

gulp.task('html', function () {
  gulp.src(paths.html)
    .pipe(browserSync.stream());
    // .pipe(reload({ stream: true }));
});


// ////////////////////////////////////////////////
// Browser-Sync Tasks
// // /////////////////////////////////////////////

gulp.task('browserSync', function () {
  browserSync.init({
    // files: ["./css/*.css"],
    proxy: 'localhost/ais-we-all/app',
    startPath: '/introduction.php',
    // server: './app',
    port: 8080
  });
  // browserSync({
  //   injectChanges: true,
  //   proxy: '127.0.0.1/aph-motorshow/app',
  //   port: 8080
  // });
});


// /////////////////////////////////////////////////
// PHP task
// ////////////////////////////////////////////////

gulp.task('php', function () {
  connectPHP.server({ base: './', keepalive: true, hostname: 'localhost', port: 8080, open: false });
});

// Note: no es6 to compile
gulp.task('scripts', function (done) {
  var bundledStream = through();

  bundledStream
    .pipe(source('app.js'))
    .pipe(rename({ extname: '.bundle.js' }))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(jsDist));
    // .pipe(reload({ stream: true }));

  glob('./app/js/**/*.js', function (err, entries) {
    if (err) done(err);
    var b = browserify({
      entries: entries,
      debug: true,
      transform: [[babelify, {
        presets: ['env']
      }]]
    });
    b
      .bundle()
      .pipe(bundledStream);
    return bundledStream;
  });

  // Workaround
  return gulp.src(['./app/dist/*.js'])
    .pipe(reload({ stream: true }));
    // .pipe(reload());

});

// Note: no es6 to compile
// gulp.task('scripts', function (done) {
//
//   glob(paths.script[0], function (err, files) {
//     if (err) done(err);
//     var tasks = files.map(function (entry) {
//       return browserify({
//         entries: [entry]
//       })
//         .transform(babelify, {
//           presets: ['env']
//         })
//         .bundle()
//         .pipe(source(entry))
//         .pipe(rename({ extname: '.bundle.js' }))
//         .pipe(buffer())
//         .pipe(sourcemaps.init({ loadMaps: true }))
//         .pipe(uglify())
//         .pipe(sourcemaps.write('./'))
//         .pipe(gulp.dest(jsDist))
//         .pipe(reload({ stream: true }));
//     });
//     es.merge(tasks).on('end', done);
//
//   });
//
//   // return gulp.src(paths.script)
//
//   // .pipe(coffee())
//   // .pipe(gulp.dest('js'))
//
//     // .pipe(reload({ stream: true }));
//
// });



gulp.task('watcher', function () {


  // Note: no es6 to compile
  // gulp.watch(paths.script, ['scripts']);
  gulp.watch(paths.script, ['scripts']);

  gulp.watch(paths.html, ['html']);

  gulp.watch(paths.css, ['mincss']);
  // Note: trying to fix style not load for non-index.php
  //   .on('change', function () {
  //     browserSync.stream();
  // });

});

// gulp.task('default', ['watcher', 'browserSync', 'php']);

gulp.task('default', ['watcher', 'browserSync']);
// gulp.task('default',  gulp.parallel('watcher', 'browserSync'));
